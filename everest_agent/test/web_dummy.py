import random
import signal
import time
import socket

from tornado import gen
from tornado import ioloop
from tornado import web

class WebHandler(web.RequestHandler):
    @gen.coroutine
    def get(self, cat, text):
        if cat == 'slow':
            yield gen.sleep(1)
        self.finish('<html>%s</html>' % text)

def signal_handler(signum, frame):
    ioloop.IOLoop.current().stop()

def main():
    random.seed(0)
    signal.signal(signal.SIGINT, signal_handler)
    app = web.Application([
        (r"/([^/]+)/(.*)\.html", WebHandler)
    ])
    while True:
        port = random.randint(11100, 11150)
        try:
            app.listen(port)
            break
        except socket.error:
            continue
    print port
    with open('task.endpoint', 'w') as f:
        f.write('localhost:%d' % port)
    ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
