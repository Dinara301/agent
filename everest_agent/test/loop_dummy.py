#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import time 
import sys

sleepTime = float(sys.argv[1])
inFile = sys.argv[2]
outFile = sys.argv[3]
lastResult = None

while True:
    time.sleep(sleepTime)
    try:
        with open(inFile, 'r') as f:
            nums = map(int, f.read().split())
            result = sum(nums)
    except IOError:
        continue
    if lastResult != result:
        with open(outFile, 'w') as f:
            f.write(str(result))
        lastResult = result
