# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import unittest
import shutil
import os

from everest_agent.cache import Cache

SHA = {
    'tree.tar.gz' : '7b1ad243795bb8d4a32a6403bf6ac1e77783e695'
}

class CacheTest(unittest.TestCase):
    def setUp(self):
        if os.path.isdir('testcache'):
            shutil.rmtree('testcache')
        self.cache = Cache('testcache', 1500)

    def tearDown(self):
        self.cache.shutdown()
        try:
            shutil.rmtree('testcache')
        except OSError:
            pass

    def myPut(self, path):
        p = self.cache.putFile()
        with open(path, 'r') as f:
            p.write(f.read())
        return p.close()

    def isCached(self, sha1):
        self.assertTrue(self.cache._isCached(sha1))

    def notCached(self, sha1):
        self.assertFalse(self.cache._isCached(sha1))

    def testNoActions(self):
        pass

    def testPut(self):
        self.notCached(SHA['tree.tar.gz'])
        sha1, path = self.myPut('tree.tar.gz')
        self.assertEqual(SHA['tree.tar.gz'], sha1)
        self.isCached(SHA['tree.tar.gz'])
        self.assertTrue(os.path.isfile(path))
        self.assertEqual(path, self.cache.getFile(sha1))

    def testDrop(self):
        sha1, path = self.myPut('tree.tar.gz')
        self.isCached(sha1)
        self.cache.dropFile(sha1)
        self.notCached(sha1)

    def testMaxSize(self):
        self.cache = Cache('testcache', 400)
        hash1, path1 = self.myPut('tree.tar.gz')
        self.isCached(hash1)
        hash2, path2 = self.myPut('README.md')
        self.isCached(hash2)
        self.notCached(hash1)

    def testATimeStorage(self):
        hash1, path1 = self.myPut('tree.tar.gz')
        hash2, path2 = self.myPut('README.md')
        self.isCached(hash1)
        self.isCached(hash2)
        self.cache.shutdown()
        # make FS atimes order different from atimes saved to file
        os.utime(path2, None)
        os.utime(path1, None)
        self.cache = Cache('testcache', 1100)
        self.isCached(hash1)
        self.isCached(hash2)
        hash3, path3 = self.myPut('runtests.sh')
        self.isCached(hash3)
        self.isCached(hash2)
        self.notCached(hash1)

if __name__ == '__main__':
    unittest.main()
