#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import time 
import sys
import subprocess

from constants import *

#~ print "This should be in STDOUT:", sys.argv[1]
#~ sys.stderr.write("This should be in STDERR: ")
if sys.argv[1] != '-1':
    sys.stdout.write(sys.argv[1])
sys.stderr.write(sys.argv[2])
my_time = float(sys.argv[3])
child_time = int (sys.argv[4])
returncode = int (sys.argv[5])
for i in range(6, len(sys.argv)):
    with open(sys.argv[i], 'rb') as f:
        sys.stdout.write(f.read())
if child_time > 0:
    subprocess.Popen(["python", DUMMY_PATH, "0", "0", str(child_time), "0", "0"])

time.sleep(my_time)
exit(returncode)
