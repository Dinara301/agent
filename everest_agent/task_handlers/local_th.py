#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import signal
import subprocess
import shlex
import json
from everest_agent.exceptions import TaskException
import time

def create(config):
    return LocalTaskHandler(config)

class LocalTaskHandler:
    def __init__(self,config):
        self.check_time = config['checkTime']

    def start_child(self, command, dir, env):
        if not os.access(dir, os.F_OK):
            os.makedirs(dir)
        if not os.access(dir, os.W_OK):
            raise TaskException("Requested directory is not writable")

        # override and uset environment variables
        task_env = os.environ.copy()
        for k, v in env.iteritems():
            if v is None:
                del task_env[k]
            else:
                task_env[k] = v

        with open(os.path.join(dir,"stdout"),"w") as std_out:
            with open(os.path.join(dir,"stderr"),"w") as std_err:
                try:
                    if os.name == 'posix':
                        p = subprocess.Popen(command, stdin = subprocess.PIPE,
                                             stdout = std_out, stderr = std_err,
                                             cwd = dir, env = task_env, preexec_fn = os.setpgrp)
                    elif os.name == 'nt':
                        p = subprocess.Popen(command, stdin = subprocess.PIPE,
                                             stdout = std_out, stderr = std_err,
                                             cwd = dir, env = task_env,
                                             creationflags = subprocess.CREATE_NEW_PROCESS_GROUP)
                    else:
                        raise TaskException("Unsupported OS API")
                except OSError, e:
                    raise TaskException("Unable to execute requested program (%s)" % e)
        return p
        
    def killpg(self, p):
        if os.name == 'posix':
            try:
                os.killpg(p.pid, signal.SIGTERM)
                timeout = 5
                while p.poll() is None and timeout > 0:
                    time.sleep(1)
                    timeout -= 1
                if p.returncode is None:
                    os.killpg(p.pid, signal.SIGKILL)
            except OSError: # nothing to kill
                pass
        elif os.name == 'nt':
            try:
                p.send_signal(signal.CTRL_BREAK_EVENT)
                timeout = 5
                while p.poll() is None and timeout > 0:
                    time.sleep(1)
                    timeout -= 1
                if p.returncode is None:
                    p.kill()
            except WindowsError:
                pass

    def submit(self, command, dir, env = {}):
        s =  shlex.split(command.encode('ascii')) #encode part is for python 2.6
        return self.start_child(s, dir, env)
        
    def get_state(self, p):
        p.poll()
        if p.returncode is None:
            return "RUNNING"

        self.cancel(p)

        if p.returncode == 0:
            return "COMPLETED"
        elif p.returncode < 0:
            return "CANCELED"
        return "FAILED ExitCode=" + str(p.returncode)

    def cancel(self, p):
        self.killpg(p)

    # this get_slots is never called so no need to port
    def get_slots(self):
        try:
            return os.sysconf('SC_NPROCESSORS_ONLN') #~Linux only
        except (ValueError, OSError, AttributeError):
            return -1

    def get_task_stat(self,pid):
        stat = {}
        return json.dumps(stat)

    def get_type(self):
        return 'local'
