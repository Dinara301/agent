#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import subprocess
import re
import json
import xml.dom.minidom
from everest_agent.exceptions import TaskException

def create(config):
    return SgeTaskHandler(config)

class SgeTaskHandler:
    def __init__(self,config):
        if 'queue' in config:
            self.queue = config['queue']
        else:
            self.queue = None

        self.job_states = {
            'd'  : 'FAILED',    #An error occurred with the job
            'Eqw': 'FAILED',    #An error occurred with the job
            'Ew' : 'FAILED',    #An error occurred with the job
            'h'  : 'QUEUED',    #Job is hold
            'r'  : 'RUNNING',   #Job is running
            'R'  : 'FAILED',    #Job is restarted
            'Rr' : 'FAILED',    #Job is restarted
            's'  : 'SUSPENDED', #Job is suspended
            'S'  : 'SUSPENDED', #Job is suspended
            't'  : 'QUEUED',    #Job is transfering
            'T'  : 'QUEUED',    #Job is Threshold 
            'w'  : 'QUEUED',    #Job is waiting  
            'qw' : 'QUEUED',    #Job is waiting
        }
        
    def submit(self, command, dir, env = {}):
        if not os.access(dir, os.F_OK):  
            os.makedirs(dir) 
        job_file_str = os.path.join(os.path.abspath(dir),"jobfile")
        job_file = open (job_file_str,"w")
        
        job_file.write("#$ -wd ")
        job_file.write(os.path.abspath(dir))
        job_file.write("\n#$ -e stderr\n")
        job_file.write("#$ -o stdout\n")
        if self.queue:
            s = "#$ -q "+self.queue + "\n"
            job_file.write(s)
        
        job_file.write(command)
        
        job_file.close()
        try:
            p = subprocess.Popen(["qsub",job_file_str],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (out,er) = p.communicate()
            re_job  = re.compile(r'^Your job (\d*) .*').search(out)
            return re_job.group(1)
        except Exception as e:
            raise TaskException("Failed to submit job to SGE")

    def get_state(self,pid):
        try:
            p = subprocess.Popen("qstat | grep %s" % pid,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (out,err) = p.communicate()
            if not out:
                return 'COMPLETED'
            else:
                state = out.split()[4]
                return self.job_states[state]
        except Exception as e:
            raise e
        
    def cancel(self,pid):
        try:
            p = subprocess.Popen(["qdel", str(pid)])
            p.wait()
        except:
            pass
                
    def get_resource_state(self):
        state = {}
        state['type'] = 'torque'
        return json.dumps(state)
        
    def get_slots(self):
        try:
            p = subprocess.Popen(["qhost","-xml"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (out,err) = p.communicate()
            out_parser = xml.dom.minidom.parseString(out)
            slots = [elem.getElementsByTagName('hostvalue')[1].firstChild.data \
                for elem in out_parser.getElementsByTagName('qhost')[0].getElementsByTagName('host')]
            total_slots = sum([int(elem) for elem in slots if elem != '-']) 
            return total_slots
        except Exception as e:
            return -1
        
    def get_free_slots(self):
        try:
            p = subprocess.Popen(["qstat","-s","r","-xml"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (out,err) = p.communicate()
            out_parser = xml.dom.minidom.parseString(out) 
            used_slots = sum ([int(elem.firstChild.data) for elem in out_parser.getElementsByTagName('slots')])
            return self.get_slots() - used_slots
        except Exception as e:
            return -1

    def get_task_stat(self,pid):
        stat = {}
        return json.dumps(stat)
    
    def get_type(self):
        return 'sge'
