#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import subprocess
import re
import json
from everest_agent.exceptions import TaskException

def create(config):
    return DockerTaskHandler(config)

class DockerTaskHandler:
    def __init__(self,config):
        try:
            p = subprocess.Popen(["id","-u"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (id,er) = p.communicate()
        except Exception as e:
            raise e
        else:
            self.user = id.strip()
        self.image = config['image']
        self.mem_limit = config['mem_limit']
        
    def submit(self, command, dir, env = {}):
        task_image = self.image
        if not os.access(dir, os.F_OK):  
            os.makedirs(dir)
        else:
            if os.path.isfile(dir + '/Dockerfile'):
                # build image
                try:
                    p = subprocess.Popen(['docker', 'build', '-t', 'everest/task', dir], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                    (out,err) = p.communicate()
                    if p.returncode == 0:
                        task_image = 'everest/task'
                    else:
                        raise TaskException("Unable to build Docker image")
                except Exception as e:
                    raise TaskException("Unable to build Docker image")

        bind = dir+":/task:rw"
        task = command.encode('ascii')
        task+=" >stdout 2>stderr"

        try:
            p = subprocess.Popen(["docker","run","-d","-c=1","-m=" + self.mem_limit,"-u",self.user,"-v",bind,"-w","/task",task_image,"/bin/sh","-c",task],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (id,er) = p.communicate()
            #print id.strip()
            return id.strip()
        except Exception as e:
            raise TaskException("Unable to start container")

    def get_state(self,pid):
        try:
            p = subprocess.Popen(["docker", "inspect",str(pid)],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
            r = json.loads(resp)
        except Exception as e:
            raise TaskException("Unable to read container state")
        if r[0]['State']['Running']:
            return "RUNNING"
        elif r[0]['State']['ExitCode'] == 0:
            return "COMPLETED"
        elif r[0]['State']['ExitCode'] < 0:
            return "CANCELED"
        else:
            st = "FAILED ExitCode=" + str(r[0]['State']['ExitCode'])
            return st
        
    def cancel(self,pid):
        try:
            p = subprocess.Popen(["docker","kill", str(pid)])
            p.wait()
        except:
            pass
                
    def get_resource_state(self):
        state = {}
        state['type'] = 'docker'
        return json.dumps(state)
        
    def get_slots(self):
        pass        
    def get_free_slots(self):
        pass
    def get_task_stat(self,pid):
        stat = {}
        return json.dumps(stat)

    def get_type(self):
        return 'docker'
