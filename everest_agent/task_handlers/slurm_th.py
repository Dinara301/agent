#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import subprocess
import re
import json
import logging
from everest_agent.exceptions import TaskException

def create(config):
    return SlurmTaskHandler(config)

class SlurmTaskHandler:
    def __init__(self,config):
        if 'queue' in config:
            self.queue = config['queue']
        else:
            self.queue = None
        if 'cpus_per_task' in config:
            self.cpus = config['cpus_per_task']
        else:
            self.cpus = 1
        self.logger = logging.getLogger("MC2-Client.SlurmTaskHandler")
    
    def submit(self, command, dir, env = {}):
        if not os.access(dir, os.F_OK):  
            os.makedirs(dir) 
        job_file_str = os.path.join(os.path.abspath(dir),"jobfile")
        #~ print job_file_str
        job_file = open (job_file_str,"w")
        
        job_file.write("#!/bin/sh\n")
        job_file.write("#SBATCH -D %s\n" % os.path.abspath(dir))
        job_file.write("#SBATCH -e stderr\n")
        job_file.write("#SBATCH -o stdout\n")
        if self.queue:
            job_file.write("#SBATCH -p %s\n" % self.queue)
        job_file.write("#SBATCH -c %d\n" % self.cpus)
        
        job_file.write(command)
        
        job_file.close()
        
        try:
            os.chmod(job_file_str,0777)
            p = subprocess.Popen(["sbatch",job_file_str],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (id,er) = p.communicate()
            #~ id = subprocess.check_output(["qsub",job_file_str]) #И здесь нужен Питон 2.7
            return int(id.split(' ')[-1])
        except Exception as e:
            #~ raise e
            raise TaskException("Unable to initiate SLURM")

    def get_state(self,pid):
        cmd = "scontrol show job %s" % str(pid)
        state = None
        try:
            p = subprocess.Popen(cmd.split(),stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,err) = p.communicate()
            if err == "":
                try:
                    r = re.search("JobState=(\w+)",resp)
                    s = r.group(1)
                    if (s == 'RUNNING') or (s == 'COMPLETING'):
                        state = "RUNNING"
                    elif (s == 'PENDING') or (s == 'CONFIGURING') or (s == 'SUSPENDED'):
                        state = "QUEUED"
                    elif (s == 'COMPLETED'):
                        state = "DONE"
                    elif (s == 'CANCELLED'):
                        state = "CANCELLED"
                    elif (s == 'FAILED'):
                        r = re.search("ExitCode=(\d+):(\d+)",resp)
                        exit_code = r.group(1)
                        state = "FAILED ExitCode=" + exit_code
                    else:
                        state = "FAILED"
                except Exception:
                    raise TaskException("Failed to parse scontrol response: %s" % resp)
            else:
                raise TaskException("Got error from scontrol command: %s" % err)
        except Exception as e:
            raise TaskException("Failed to invoke scontrol command: %s" %cmd)
        return state
        
    def cancel(self,pid):
        try:
            p = subprocess.Popen(["scancel", str(pid)])
            p.wait()
        except:
            pass

    def get_slots(self):
        try:
            p = subprocess.Popen(["sinfo", '-p', self.queue, '-o','"%C"'  ],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
            #~ resp = subprocess.check_output(["qstat", "-f",str(pid)])
        except Exception as e:
            #~ raise e
            raise TaskException("Unable to get the status of the job")
        num_m = re.search("\d+/\d+/\d+/(?P<slots>\d+)", resp)
        return int(num_m.group('slots'))

    def get_free_slots(self):
        try:
            p = subprocess.Popen(["sinfo", '-p', self.queue, '-o','"%C"'  ],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
            #~ resp = subprocess.check_output(["qstat", "-f",str(pid)])
        except Exception as e:
            #~ raise e
            raise TaskException("Unable to get the status of the job")
        num_m = re.search("\d+/(?P<slots>\d+)/\d+/\d+", resp)
        return int(num_m.group('slots'))

    
    def get_resource_state(self):
        state = {}
        state['type'] = 'slurm'
        return json.dumps(state)
        
    def get_task_stat(self,pid):
        stat = {}
        return json.dumps(stat)
        
    def get_type(self):
        return 'slurm'
