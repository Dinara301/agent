# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import tempfile
import hashlib
import time
import operator

class Cache(object):
    def __init__(self, cacheDir, cacheSize):
        self.cacheDir = cacheDir
        self.maxSize = cacheSize
        self.tmpDir = os.path.join(self.cacheDir, 'tmp')
        self.puts = {}
        self._load()

    def _getPath(self, sha1):
        return os.path.join(self.cacheDir, sha1)

    def _load(self):
        self.size = 0
        self.paths = {}
        for root, dirs, files in os.walk(self.cacheDir):
            if root == self.tmpDir:
                continue
            for filename in files:
                if filename == 'atimes.txt':
                    continue
                s = os.stat(os.path.join(root, filename))
                self.paths[filename] = {
                    'path' : os.path.join(root, filename),
                    'size' : s.st_size,
                    'atime' : float(s.st_atime)
                }
                self.size += s.st_size
        try:
            with open(os.path.join(self.cacheDir, 'atimes.txt'), 'rb') as f:
                for l in f.readlines():
                    spl = l.strip().split()
                    if spl[0] in self.paths:
                        self.paths[spl[0]]['atime'] = float(spl[1])
        except IOError:
            pass

    def shutdown(self):
        if self.paths:
            with open(os.path.join(self.cacheDir, 'atimes.txt'), 'wb') as f:
                for k, v in self.paths.iteritems():
                    f.write('%s %f\n' % (k, v['atime']))

    def getFile(self, sha1):
        if not sha1 in self.paths:
            return ''
        self.paths[sha1]['atime'] = time.time()
        return self.paths[sha1]['path']

    def _isCached(self, sha1):
        return sha1 in self.paths

    def putFile(self):
        "Return file-like object to be used by the client for writing file contents"
        if not os.path.exists(self.tmpDir):
            os.makedirs(self.tmpDir)
        f = tempfile.NamedTemporaryFile(delete=False, dir=self.tmpDir)
        self.puts[f] = hashlib.sha1()
        return Putter(self, f)

    def dropFile(self, sha1):
        path = self.getFile(sha1)
        if path:
            os.remove(path)
            self.size -= self.paths[sha1]['size']
            assert(self.size >= 0)
            del self.paths[sha1]
            return True
        return False

    def _write(self, f, data):
        assert(f in self.puts)
        f.write(data)
        self.puts[f].update(data)

    def _close(self, f):
        assert(f in self.puts)
        tmpName = f.name
        f.close()
        sha1 = self.puts[f].hexdigest()
        del self.puts[f]
        if sha1 in self.paths:
            os.remove(tmpName)
            self.paths[sha1]['atime'] = time.time()
            return sha1, self.paths[sha1]['path']
        size = os.path.getsize(tmpName)
        if size + self.size > self.maxSize:
            self._freeSomeSpace(size)
        path = self._getPath(sha1)
        os.rename(tmpName, path)
        self.paths[sha1] = {
            'path' : path,
            'size' : size,
            'atime' : time.time()
        }
        self.size += size
        return (sha1, path)

    def _cleanup(self, f):
        assert(f in self.puts)
        tmpName = f.name
        f.close()
        del self.puts[f]
        os.remove(tmpName)

    def _freeSomeSpace(self, minFreeSpace):
        # free space for at least new file size or half of the cache
        # goalSize = min(self.maxSize / 2, max(0, self.maxSize - minFreeSpace))
        # free space for the new file only
        goalSize = max(0, self.maxSize - minFreeSpace)
        byTime = sorted([(v['atime'], k) for k, v in self.paths.iteritems()],
                        key=operator.itemgetter(0))
        for t, sha1 in byTime:
            if self.size <= goalSize or self.size <= 0:
                break
            self.dropFile(sha1)

class Putter(object):
    def __init__(self, cache, f):
        self.f = f
        self.cache = cache

    def write(self, data):
        self.cache._write(self.f, data)

    def close(self):
        return self.cache._close(self.f)
