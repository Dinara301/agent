# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os

from . import qt

class PopupTask(qt.QtGui.QFrame):
  """
  Simple popup to show extended task info.

  Uses QFrame with border to avoid ugliness on flat Qt styles (e.g. windows classic).
  """
  _WIDTH = 300
  def __init__(self, parent, details, options):
    """
    Init new PopupTask instance.
    """
    qt.QtGui.QFrame.__init__(self, parent)
    self.setFrameShape(qt.QtGui.QFrame.Box)
    self.setFrameShadow(qt.QtGui.QFrame.Raised)
    self.setWindowFlags(qt.QtCore.Qt.Popup)
    layout = qt.QtGui.QVBoxLayout()
    form_layout = qt.QtGui.QFormLayout()
    form_layout.setContentsMargins(0, 0, 0, 0)
    def add_row(name, value):
      value = str(value)
      view = qt.QtGui.QLineEdit(value)
      view.setReadOnly(True)
      if not value:
        view.setText(self.tr("<empty>"))
        view.setEnabled(False)
      form_layout.addRow(name, view)
    add_row(self.tr("Task ID"), details["id"])
    add_row(self.tr("State"), details["state"].capitalize())
    add_row(self.tr("Name"), details["name"])
    add_row(self.tr("Description"), details["description"])
    add_row(self.tr("Submit time"), qt.QtCore.QDateTime.fromString(details["submitUTC"], qt.QtCore.Qt.ISODate).toLocalTime().toString("dd-MM-yy HH:mm:ss"))
    add_row(self.tr("Client token"), details["clientToken"])
    add_row(self.tr("Command"), details["command"])
    add_row(self.tr("Directory"), os.path.join(options.workdir, details["dir"]))

    layout.addWidget(qt.QtGui.QLabel(self.tr("Task details")))
    layout.addLayout(form_layout)

    self.setLayout(layout)
    self.setFixedWidth(self._WIDTH)

