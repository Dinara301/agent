# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import datetime

from . import qt
from . import resource
from . import popup_task

class TabTasks(qt.QtGui.QWidget):
  """
  Tasks tab - see current running tasks and their status, control their execution (at the very least, interrupt).
  """
  _REFRESH_ICON = qt.QtGui.QIcon(resource.get("icon-refresh.png"))

  def __init__(self, model):
    """
    Init new TabTasks instance.
    """
    qt.QtGui.QWidget.__init__(self)
    self.__model = model
    self.__task_widgets = []

    self.__animate_timer = qt.QtCore.QTimer()
    self.__animate_timer.setInterval(1000)
    self.__animate_timer.timeout.connect(self.__animate)

    layout = qt.QtGui.QVBoxLayout()
    header_layout = qt.QtGui.QHBoxLayout()
    footer_layout = qt.QtGui.QHBoxLayout()
    header_layout.setContentsMargins(0, 0, 0, 0)
    footer_layout.setContentsMargins(0, 0, 0, 0)

    title_label = qt.QtGui.QLabel(self.tr("Current tasks"))
    self.__reload_button = qt.QtGui.QPushButton(self._REFRESH_ICON, "")
    self.__reload_button.setToolTip(self.tr("Refresh tasks list"))
    header_layout.addWidget(title_label)
    header_layout.addStretch(1)
    header_layout.addWidget(self.__reload_button)

    self.__scroll_area = qt.QtGui.QScrollArea()
    self.__scroll_area.setWidgetResizable(True)
    self.__scroll_widget = qt.QtGui.QWidget()
    self.__scroll_widget.setSizePolicy(qt.QtGui.QSizePolicy.Minimum, qt.QtGui.QSizePolicy.Preferred)
    self.__scroll_layout = qt.QtGui.QVBoxLayout()
    full_scroll_layout = qt.QtGui.QVBoxLayout()
    full_scroll_layout.setContentsMargins(0, 0, 0, 0)
    self.__scroll_layout.setContentsMargins(5, 5, 5, 5)
    full_scroll_layout.addLayout(self.__scroll_layout)
    full_scroll_layout.addStretch(1)
    self.__scroll_widget.setLayout(full_scroll_layout)
    self.__scroll_area.setWidget(self.__scroll_widget)

    self.__summary_label = qt.QtGui.QLabel()
    footer_layout.addStretch()
    footer_layout.addWidget(self.__summary_label)

    layout.addLayout(header_layout)
    layout.addWidget(self.__scroll_area, 1)
    layout.addLayout(footer_layout)
    self.setLayout(layout)

    self.__reload_button.clicked.connect(self.__model.reload)
    self.__model.tasks_changed.connect(self.__update_tasks)
    self.__model.backend.running_changed.connect(self.__on_running_changed)

    self.__on_running_changed(self.__model.backend.is_running)
    self.__update_tasks()

  def hideEvent(self, event):
    """
    Stop task widgets update when panel is hidden.
    """
    self.__animate_timer.stop()
    super(TabTasks, self).hideEvent(event)

  def showEvent(self, event):
    """
    Restart task widgets update when panel is shown.
    """
    self.__animate()
    self.__animate_timer.start()
    super(TabTasks, self).showEvent(event)

  def __animate(self):
    for w in self.__task_widgets:
      w.animate()

  def __on_running_changed(self, status):
    self.__reload_button.setEnabled(status)

  def __update_tasks(self):
    """
    Update task widgets list according to current model state.
    """
    tasks = self.__model.get_tasks()

    filtered_widgets = []
    existing_ids = set()
    for widget in self.__task_widgets:
      widget_id = widget.task_id
      if widget_id not in tasks:
        self.__scroll_layout.removeWidget(widget)
        widget.deleteLater()
      else:
        widget.update_data(tasks[widget_id])
        filtered_widgets.append(widget)
        existing_ids.add(widget_id)
    self.__task_widgets = filtered_widgets

    to_create = set(tasks.keys()) - existing_ids
    for task_id in to_create:
      widget = _TaskWidget(tasks[task_id])
      widget.stop_requested.connect(self.__on_stop_requested, qt.QtCore.Qt.QueuedConnection)
      widget.details_requested.connect(self.__on_details_requested, qt.QtCore.Qt.QueuedConnection)
      self.__scroll_layout.addWidget(widget)
      self.__task_widgets.append(widget)

    running_count = len(filter(lambda t: t["state"] == "RUNNING", tasks.itervalues()))
    total_count = len(tasks)
    self.__summary_label.setText(self.tr("{:d} running, {:d} total").format(running_count, total_count))


  def __on_stop_requested(self, task_id):
    """
    Stop/cancel task execution.
    """
    if qt.QtGui.QMessageBox.question(self, self.tr("Stop task"),
                                     self.tr("Are you sure?"),
                                     qt.QtGui.QMessageBox.Yes | qt.QtGui.QMessageBox.No) == qt.QtGui.QMessageBox.Yes:
      try:
        self.__model.backend.task_delete(task_id)
        self.__model.reload()
      except KeyError:
        qt.QtGui.QMessageBox.warning(self, self.tr("Stop task"), self.tr("Task already finished"))

  def __on_details_requested(self, task_id, position):
    """
    Show task details in a popup.
    """
    try:
      details = self.__model.backend.task_info(task_id)
      popup = popup_task.PopupTask(self, details, self.__model.options)
      popup.setAttribute(qt.QtCore.Qt.WA_DeleteOnClose)
      popup.move(position)
      popup.show()
    except KeyError:
      qt.QtGui.QMessageBox.warning(self, self.tr("Task details"), self.tr("Task does not exist"))


class _TaskWidget(qt.QtGui.QFrame):
  """
  Widget representing single active task in tasks panel.
  """
  _ITEM_HEIGTH = 45
  _TEXT_WIDTH = 300
  _DETAILS_ICON = qt.QtGui.QIcon(resource.get("icon-question-sign.png"))

  _TASK_CATEGORIES = {
    "ACCEPTED": qt.PC_FINE,
    "STAGED_IN": qt.PC_FINE,
    "QUEUED": qt.PC_WARN,
    "RUNNING": qt.PC_FINE,
    "COMPLETED": qt.PC_FINE,
    "STAGED_OUT": qt.PC_FINE,
    "DONE": qt.PC_FINE,
    "FAILED": qt.PC_FAIL,
    "CANCELED": qt.PC_WARN,
    "DELETED": qt.PC_FAIL
  }

  stop_requested = qt.Signal(str)
  details_requested = qt.Signal(str, qt.QtCore.QPoint)

  def __init__(self, task_data):
    """
    Init new _TaskWidget instance.
    """
    qt.QtGui.QFrame.__init__(self)
    self.setSizePolicy(qt.QtGui.QSizePolicy.Minimum, qt.QtGui.QSizePolicy.Fixed)
    self.setFixedHeight(self._ITEM_HEIGTH)
    self.setFrameShape(qt.QtGui.QFrame.Panel)
    self.setFrameShadow(qt.QtGui.QFrame.Raised)
    self.__data = None

    layout = qt.QtGui.QHBoxLayout()
    layout.setContentsMargins(5, 0, 5, 0)

    text_layout = qt.QtGui.QVBoxLayout()
    text_layout.setContentsMargins(0, 0, 0, 0)

    self.__details_button = qt.QtGui.QPushButton(self._DETAILS_ICON, "")
    self.__details_button.setToolTip(self.tr("Details..."))
    self.__details_button.setFlat(True)
    self.__name_label = qt.QtGui.QLabel()
    self.__name_label.setFixedWidth(self._TEXT_WIDTH)
    self.__submit_label = qt.QtGui.QLabel()
    self.__submit_label.setFixedWidth(self._TEXT_WIDTH)
    self.__state_label = qt.QtGui.QLabel()
    self.__state_label.setSizePolicy(qt.QtGui.QSizePolicy.Minimum, qt.QtGui.QSizePolicy.Fixed)
    # TODO: idea - change text according to task state
    self.__stop_button = qt.QtGui.QPushButton(self.tr("Cancel"))
    self.__stop_button.setFixedWidth(80)

    self.__details_button.clicked.connect(self.__details)
    self.__stop_button.clicked.connect(self.__stop)

    self.update_data(task_data)

    text_layout.addWidget(self.__name_label)
    text_layout.addWidget(self.__submit_label)

    layout.addWidget(self.__details_button)
    layout.addLayout(text_layout, 1)
    layout.addWidget(self.__state_label)
    layout.addStretch(1)
    layout.addWidget(self.__stop_button)

    self.setLayout(layout)

  @property
  def task_id(self):
    """
    Get task id.
    """
    return self.__data["id"]

  def update_data(self, task_data):
    """
    Update task data.
    """
    if task_data == self.__data:
      return
    if self.__data:
      assert self.task_id == task_data["id"]

    task_name = task_data["name"] or "Task#{}".format(task_data["id"])
    self.__set_elided_text(self.__name_label, task_name)
    self.__display_submit_info(task_data["submitUTC"], task_data["clientToken"])
    self.__state_label.setText(task_data["state"].capitalize())
    self.__state_label.setProperty(qt.PC_PROPERTY, self._TASK_CATEGORIES[task_data["state"]])
    self.__stop_button.setEnabled(task_data["state"] not in {"CANCELED"})

    # ugly kludge from Qt docs to update element style on custom property change
    self.__state_label.style().unpolish(self.__state_label)
    self.__state_label.style().polish(self.__state_label)
    self.__state_label.update()

    self.__data = task_data

  def animate(self):
    """
    'Animate' widget - update time since submit.
    """
    self.__display_submit_info(self.__data["submitUTC"], self.__data["clientToken"])

  def __display_submit_info(self, time_string, client_string):
    """
    Generate task submit info string and set it to corresponding label.
    """
    utc_time = qt.QtCore.QDateTime.fromString(time_string, qt.QtCore.Qt.ISODate).toUTC().toMSecsSinceEpoch()
    utc_now = qt.QtCore.QDateTime.currentDateTimeUtc().toMSecsSinceEpoch()
    delta_as_string = self.__time_interval_to_string((utc_now - utc_time) / 1000)
    self.__submit_label.setText(self.tr("{:s} ago from {}").format(delta_as_string, client_string))

  def __time_interval_to_string(self, seconds):
    """
    Convert time delta in seconds to human-friendly string.

    Examples:
      3 -> 3 seconds
      30 -> 30 seconds
      60 -> 1m 0s
      122 -> 2m 2s
      3600 -> 1h 0m
      7654 -> 2h 7m
    """
    seconds = int(seconds)
    if seconds < 60:
      return "{:d} {:s}".format(seconds, self.tr("seconds"))
    if seconds < 3600:
      return "{:d}{:s} {:d}{:s}".format(seconds / 60, self.tr("m"), seconds % 60, self.tr("s"))
    return "{:d}{:s} {:d}{:s}".format(seconds / 3600, self.tr("h"), (seconds % 3600) / 60, self.tr("m"))

  def __stop(self):
    """
    Emit stop task request.

    Can't process it right here as this widget may disappear at any moment.
    """
    self.stop_requested.emit(self.task_id)

  def __details(self):
    """
    Emit task details request.

    Can't process it right here as this widget may disappear at any moment.
    """
    button_center_absolute_position = self.__details_button.mapToGlobal(qt.QtCore.QPoint(self.__details_button.width() / 2, self.__details_button.height() / 2))
    self.details_requested.emit(self.task_id, button_center_absolute_position)

  def __set_elided_text(self, label, text):
    """
    Set potentially long text to label of fixed width, eliding it with ellipsis if required.
    """
    label.setToolTip(text)
    width = label.width()
    font_metrics = qt.QtGui.QFontMetrics(label.font())
    if font_metrics.width(text) > width:
      text = font_metrics.elidedText(text, qt.QtCore.Qt.ElideRight, width, qt.QtCore.Qt.TextShowMnemonic)
    label.setText(text)

