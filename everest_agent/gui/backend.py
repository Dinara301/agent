# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import sys
import os
import traceback
import logging
import socket
import errno
import json
import httplib

from tornado import httpclient

from .. import constants
from . import qt
from . import log_file_watcher
from . import resource


class Backend(qt.QtCore.QObject):
  """
  Manages connection to agent and requests to its admin interface.

  Has quite complicated start/stop logic to enable correct behavior of start/stop buttons.
  There are two 'flags' with corresponding events - 'uninitialized' and 'running'.

  When agent is 'uninitialized' but not 'running':
  * you cannot call stop yet (or start again) - start/stop button should be disabled
  * you cannot call agent API yet

  We have at least two 'start' buttons, so just disabling the button on push is not an option.

  When we call start, we get to 'uninitialized' state immediately, than to 'running'.
  """
  # number of retries connecting to agent
  _CONNECT_RETRIES = 100
  # inteval between connections (ms)
  _CONNECT_INTERVAL = 100

  # Agent start/stop notify
  running_changed = qt.Signal(bool)
  # Agent starting/stopping notify
  uninitialized_changed = qt.Signal(bool)
  # Start failed
  start_failed = qt.Signal(str)

  def __init__(self, agent_config, options):
    """
    Init new backend instance.
    """
    qt.QtCore.QObject.__init__(self)
    self.__config = agent_config
    self.__options = options
    self.__log = logging.getLogger(self.__class__.__name__)
    self.__client = httpclient.HTTPClient()
    self.__log_watcher = log_file_watcher.LogFileWatcher()
    self.__is_running = False
    self.__is_uninitialized = False
    self.__connect_attempts = 0
    self.__process = None
    self.__closing = False

  def start(self):
    """
    Start agent process and connect to its control API.
    Emits 'running_changed' only after agent actually started and ready.
    """
    if self.is_running:
      raise Exception("agent is already running")
    if self.is_uninitialized:
      raise Exception("agent is uninitialized")
    self.__connect_attempts = 0
    self.__update_uninitialized(True)
    self.__process = qt.QtCore.QProcess()
    self.__process.started.connect(self.__started)
    self.__process.finished.connect(self.__finished)
    self.__process.setProcessChannelMode(qt.QtCore.QProcess.MergedChannels)
    self.__process.setWorkingDirectory(self.__options.workdir)
    self.__log.debug("Agent configuration:")
    for path in self.__config.keys():
      self.__log.debug("  %s: %s", "/".join(path), self.__config.get(*path))
    self.__process.start(sys.executable, [resource.get("runner.py"), "-c", self.__config.path, "-l", self.__options.log_file])

  def stop(self, force=False):
    """
    Kill agent process.
    """
    if not force:
      if not self.is_running:
        raise Exception("agent is not running")
      if self.is_uninitialized:
        raise Exception("agent is uninitialized")
    if self.__process is not None:
      self.__update_uninitialized(True)
      # TODO: do we need graceful shutdown?
      #
      # upd 27.05.15 yes we do. On Windows:
      # [Backend] [ERROR] [2015-05-27 16:47:53] Agent exited with non-zero error code (62097)
      self.__close_process()

  @property
  def is_running(self):
    """
    Check that agent is running and fully initialized.
    """
    return self.__is_running

  @property
  def is_uninitialized(self):
    """
    Check if agent is in 'uninitialized' state, meaning starting or closing.
    """
    return self.__is_uninitialized

  @property
  def endpoint(self):
    return "{}:{}".format(socket.gethostname(), self.__config.agent_port)

  @property
  def log_file_watcher(self):
    """
    Get agent log model.
    """
    return self.__log_watcher

  def info(self):
    """
    Get basic agent info.
    """
    return self.__api_call("agent")

  def resource_info(self):
    """
    Get resource info.
    """
    return self.__api_call("resource")

  def tasks(self):
    """
    Get list of current tasks.
    """
    return self.__api_call("tasks")

  def task_info(self, task_id):
    """
    Get detailed task info.
    """
    try:
      return self.__api_call("tasks", task_id)
    except httpclient.HTTPError as ex:
      if ex.code == httplib.NOT_FOUND:
        raise KeyError(task_id)
      else:
        raise

  def task_delete(self, task_id):
    """
    Cancel task execution.
    """
    try:
      return self.__client.fetch(self.__admin_url("tasks", task_id), method="DELETE").body
    except httpclient.HTTPError as ex:
      if ex.code == httplib.NOT_FOUND:
        raise KeyError(task_id)
      else:
        raise

  def __admin_url(self, *path):
    """
    Get full url to agent control url.
    """
    return "/".join(["http://127.0.0.1:{}".format(self.__config.control_port), "control"] + list(path))

  def __api_call(self, *path):
    """
    Call agent control API.
    """
    return json.loads(self.__client.fetch(self.__admin_url(*path)).body)

  def __started(self):
    """
    Handle agent process start.
    """
    qt.QtCore.QTimer.singleShot(self._CONNECT_INTERVAL, self.__try_connect)

  def __try_connect(self):
    """
    Try to connect to agent control API.

    Calls itself in a QTimer.singleShot loop.
    """
    # process died on start, but __try_connect event is already in event loop
    if self.__process is None or self.__process.state() == qt.QtCore.QProcess.NotRunning:
      return
    self.__connect_attempts += 1
    try:
      info = self.info()
      if info["version"] != constants.AGENT_VERSION:
        self.__log.warning("Application version (%s) different from agent version (%s)", qt.QtGui.QApplication.applicationVersion(), info["version"])
      self.__process.closeReadChannel(qt.QtCore.QProcess.StandardOutput)
      self.__process.closeReadChannel(qt.QtCore.QProcess.StandardError)
      self.__log_watcher.start(self.__options.log_file)
      self.__update_running(True)
      self.__update_uninitialized(False)
    except socket.error as ex:
      if self.__connect_attempts > self._CONNECT_RETRIES:
        self.__log.error("Agent API access timeout exceeded")
        self.__close_process()
      elif ex.errno != errno.ECONNREFUSED:
        self.__log.error("Unexpected error code (%s) while accessing agent API", errno.errorcode[ex.errno])
        self.__close_process()
      else:
        qt.QtCore.QTimer.singleShot(self._CONNECT_INTERVAL, self.__try_connect)
    except Exception as ex:
      self.__log.error("Unexpected error while statring the agent")
      self.__log.debug(traceback.format_exc())
      self.__close_process()

  def __finished(self, exit_code):
    """
    Handle agent process exit.
    """
    # ignore exit code in case of correct closing by GUI
    if (not self.__closing) and exit_code:
      if not self.is_running:
        # process died on start
        self.start_failed.emit(str(self.__process.readAll()))
      self.__log.error("Agent exited with non-zero error code (%s)", exit_code)
    # it's important to shut down log watcher, as its internal monitor thread will block application exit
    #
    # as LogFileWatcher starts only on successful agent start, we need to check if it's running
    if self.__log_watcher.is_running:
      self.__log_watcher.stop()
    self.__update_running(False)
    self.__update_uninitialized(False)
    # dispose of QProcess instance
    #
    # self.__process = None there will lead to coredump
    # (as python will delete an object immediately while we still processing signal from it)
    qt.QtCore.QTimer.singleShot(0, self.__dispose_process)

  def __update_uninitialized(self, status):
    """
    Update agent uninitialized state and emit corresponding signal.
    """
    self.__is_uninitialized = status
    self.uninitialized_changed.emit(status)

  def __update_running(self, status):
    """
    Update agent running state and emit corresponding signal.
    """
    self.__is_running = status
    self.__log.info("Agent started" if status else "Agent stopped")
    self.running_changed.emit(status)

  def __close_process(self):
    """
    Forcefully close agent process.
    """
    assert self.__process is not None
    # not-too-nice kludge to distinguish correct closing by GUI
    try:
      self.__closing = True
      self.__process.close()
    finally:
      self.__closing = False

  def __dispose_process(self):
    """
    After process is finished, remove QProcess instance.
    """
    # as this func is run through event loop, agent can be already restarted
    if self.__is_running or self.__is_uninitialized:
      return
    self.__process = None
