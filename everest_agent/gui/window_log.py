# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from . import qt
from . import log_area
from . import log_file_watcher

class WindowLog(qt.QtGui.QDialog):
  """
  Configuration window.
  """
  _MIN_WIDTH = 600
  _MIN_HEIGHT = 400

  _LIST_SEPARATOR = ":"

  @staticmethod
  def show_modal(parent, log_file, log_level):
    """
    Show new log window (synchronously)
    """
    dialog = WindowLog(parent, log_file, log_level)
    dialog.setAttribute(qt.QtCore.Qt.WA_DeleteOnClose)
    return dialog.exec_()

  def __init__(self, parent, log_file, log_level):
    """
    Init new log window.
    """
    qt.QtGui.QDialog.__init__(self, parent)
    self.__log_file = log_file
    self.__watcher = log_file_watcher.LogFileWatcher()
    self.__watcher.start(self.__log_file)
    # init window properties
    self.setWindowTitle(self.tr("Log"))
    self.setMinimumSize(self._MIN_WIDTH, self._MIN_HEIGHT)
    # init layout
    layout = qt.QtGui.QVBoxLayout()
    layout.addWidget(log_area.LogArea(self.tr("Log"), self.__watcher, log_level), 1)
    layout.addLayout(self.__init_buttons())
    self.setLayout(layout)

  def __del__(self):
    self.__watcher.stop()

  def __init_buttons(self):
    """
    Get ok/cancel buttons layout.
    """
    layout = qt.QtGui.QHBoxLayout()

    button_close = qt.QtGui.QPushButton(self.tr("Close"))
    button_close.setDefault(True)

    button_close.clicked.connect(self.accept)

    layout.addStretch(1)
    layout.addWidget(button_close)

    return layout

