#
# coding: utf-8
# Copyright (C) DATADVANCE, 2010-2015
#

##
# @file
# Log file watcher
#
import logging
import threading
import time
import copy


class LogFileWatcher(object):
  """
  Instance of this class tracks log file and maintains index allowing to quickly find log lines matching filtering criteria.
  Such criteria consist from logging level and list of sources. In short the index is a list of special structures (called
  footprints) each of which represents summary of messages contained in the log file up to the specified offset. This allows
  to quickly navigate to the position of log file containing lines matching given filtering criteria.

  To watch the log file the instance spawns worker thread which monitors log file each __FILE_POLLING_DELAY_SEC seconds. Once it
  finds new lines in the file it parses them and update index.

  Log file must correspond to the following format:

    SOURCE       LOG_LEVEL     TIMESTAMP        MESSAGE
    [Application] [DEBUG] [2015-05-15 13:34:04] Initialization step 1: configure

  Index is list of footprint objects. Each footprint points to some specific line (by storing file offset) in the log file and
  contains all necessary statistics info about all the lines appeared above the pointed position.

  Here is the example explaining what index is:
    [System] [DEBUG] [15:44:24.695] Bla...
    [System] [DEBUG] [15:44:24.697] Bla...
    [System] [DEBUG] [15:44:24.991] Bla...    <-- Footprint: {"file_offset": 142,
    [Python] [INFO] [15:44:25.695] Bla...                     "sources" : {"System":[3,0,0,0,0]}}
    [Python] [WARNING] [15:44:26.315] Bla...
    [Python] [ERROR] [15:44:26.891] Bla...    <-- Footprint: {"file_offset": 284,
    [Other] [INFO] [15:44:28.103] Bla...                      "sources" : {"System":[3,0,0,0,0], "Python":[0,1,1,1,0]}}

  As it is depicted above each footprint element contains file offset pointing to the beginning of some line in log file and holds
  total number of debug/info/warning/error/fatal messages for each source.

  Obviously size of footprints collection grow together with the log file itself, so to avoid excessive memory consumption
  we remove every second footprint element from collection once its size reaches __FOOTPRINTS_SIZE_LIMIT items.
  """

  __FILE_POLLING_DELAY_SEC = 0.2
  __FOOTPRINTS_STEP_INITIAL = 128
  __FOOTPRINTS_SIZE_LIMIT = 1024

  # mapping of log markers to logging log levels
  STRING_TO_LOG_LEVEL_MAPPING = dict([(logging.getLevelName(level), level) for level in [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.FATAL]])

  def __init__(self):
    self.__worker_thread_init_condition = threading.Condition(threading.Lock())
    self.__footprints_lock = threading.Lock()
    self.__worker_thread = None

    self.__footprints = []
    self.__footprints.append(Footprint())
    self.__footprints_step = self.__FOOTPRINTS_STEP_INITIAL

  def start(self, filename):
    """
    (Re)start watching log.
    """
    self.__filename = filename
    self.__stop_thread_requested = False
    self.__worker_thread = threading.Thread(None, self.__watcher)

    self.__footprints = []
    self.__footprints.append(Footprint())
    self.__footprints_step = self.__FOOTPRINTS_STEP_INITIAL

    # wait for thread to start
    with self.__worker_thread_init_condition:
      self.__worker_thread.start()
      self.__worker_thread_init_condition.wait()

  def stop(self):
    """
    Stop watching log.
    """
    if self.__stop_thread_requested:
      raise RuntimeError("log file watcher alredy stopped")
    self.__filename = None
    self.__stop_thread_requested = True
    self.__worker_thread.join()
    self.__worker_thread = None
    # empty the footprints to avoid any 'reads' from stopped log file watcher
    self.__footprints = []
    self.__footprints.append(Footprint())

  @property
  def filename(self):
    return self.__filename

  @property
  def is_running(self):
    return self.__worker_thread is not None

  def footprint(self):
    """
    Get log file footprint: dictionary with file offset, list of sources, number of fatal, errors, warning, debug and info messages for each source.
    Example:
      {"sources": [{"name": "Service", "counters": [20, 0, 0, 0, 0]},
                   {"name": "Launcher", "counters": [3, 0, 0, 0, 0]}],
       "file_offset": 35094}
    """
    return self.__footprints[-1].todict()

  def request_data(self, line_index, line_count, sources, log_level, footprint_to_start_from):
    """
    Request 'line_count' lines matching specified filtering criteria (log level and set of sources) starting from line with
    index 'line_index'. Note, lines are numbered after filters applied.

    Method returns list of parsed log lines, e.g. list of tuples (timestamp, log_level, source, message). Note, 'log_level'
    is a number corresponding to one of logging module constants, e.g. logging.DEBUG, logging.INFO, etc...
    """

    # initialize footprint to start from: create empty if not specified or construct one from dict representation
    if footprint_to_start_from is None:
      footprint_to_start_from = Footprint()
    else:
      footprint_to_start_from = Footprint.fromdict(footprint_to_start_from)

    # copy footprints collection reducing counters of footprints (by footprint_to_start_from) and throwing away
    # footprints that reside before footprint we start from
    footprints = []
    with self.__footprints_lock:
      # start footprints collection with given one reduced by itself (all counters equal to zero but file offset is not)
      footprints.append(footprint_to_start_from.subtract_counters(footprint_to_start_from))
      for footprint in self.__footprints:
        # ignore footprints that reside before the one to start from
        if footprint.file_offset <= footprint_to_start_from.file_offset:
          continue
        subtracted_footprint = footprint.subtract_counters(footprint_to_start_from)
        # check there is no negative counters
        assert all([all(map(lambda c: c >= 0, counters)) for counters in subtracted_footprint.source_counters.values()]), (
               "specified footprint has inconsistent counters and file offset")
        footprints.append(subtracted_footprint)

    result = []
    with open(self.__filename, "r") as log_file:
      for footprint_index in xrange(1, len(footprints)):
        footprint = footprints[footprint_index]
        footprint_prev = footprints[footprint_index - 1]

        # if footprint indicates that first requested line is still not here (index of last line matching filtering
        # criteria less than requested line index), then simply skip it
        if footprint.total_lines(sources, log_level) - 1 < line_index:
          continue

        # if log file is just opened then we take previous footprint and rewind log file forward until footprint
        # counters indicate that the next line matching filtering criteria will be the first requested one
        if log_file.tell() == 0:
          start_footprint = copy.deepcopy(footprint_prev)

          # rewind to the beginning of the footprint
          log_file.seek(start_footprint.file_offset)

          # rewind forward until we reach the line previous to the one which index is specified in request
          while start_footprint.total_lines(sources, log_level) - 1 < line_index - 1:
            line = log_file.readline()
            if not line:
              assert False, "end of log file reached during searching for log line to start with"
            # parse line and update footprint
            line_timestamp, line_log_level, line_source, line_message = self.__parse_line(line)
            start_footprint.update(line_source, line_log_level, log_file.tell())
        else:
          # if section of log file pointed by current footprint contains the same number of lines matching
          # filtering criteria that the previous footprint then simply skip it (and rewind the file position)
          if (footprint.total_lines(sources, log_level) == footprint_prev.total_lines(sources, log_level)):
            log_file.seek(footprint.file_offset)
            continue

        # read lines accumulating the ones which pass filtering criteria, note we stop reading once we reach the
        # position pointed by current footprint
        while len(result) != line_count and log_file.tell() != footprint.file_offset:
          line = log_file.readline()
          if not line:
            assert False, "end of log file reached during parsing segment addressed by some footprint"

          line_timestamp, line_log_level, line_source, line_message = self.__parse_line(line)
          # if this line matches given criteria then add it into result collection
          if line_source in sources and line_log_level >= log_level:
            result.append((line_timestamp, line_log_level, line_source, line_message))

        # when we have collected requested number of lines stop iterating across footprints
        if len(result) == line_count:
          break

    assert len(result) == line_count, "log has less lines than requested"

    return result

  def __follow(self, file):
    """
    Generator that "follows" given file object continuously checking if there is new line at the end and
    yielding this line of text if so.
    """
    while not self.__stop_thread_requested:
      line = file.readline()
      if not line:
        time.sleep(self.__FILE_POLLING_DELAY_SEC)
        continue
      yield line

  def __watcher(self):
    """
    Thread function that watches log file and maintains state (statistics, cache, etc.).
    """

    # notify constructor that thread is started
    with self.__worker_thread_init_condition:
      self.__worker_thread_init_condition.notify()

    with open(self.__filename, "r") as log_file:
      lines_count = 0
      for line in self.__follow(log_file):
        # make new footprint record after each bunch of lines
        lines_count += 1
        if lines_count == self.__footprints_step:
          lines_count = 0
          with self.__footprints_lock:
            # if number of elements in footprints collection exceed the predefined limit then increase footprint step and remove every
            # second footprint element, but preserve latest one, cause we want it to contain proper statistics of the whole log file
            if len(self.__footprints) >= self.__FOOTPRINTS_SIZE_LIMIT:
              self.__footprints_step *= 2
              # remove every second element starting from the pre-last one
              del self.__footprints[-2::-2]
            self.__footprints.append(copy.deepcopy(self.__footprints[-1]))

        # parse next line and update last footprint counters
        timestamp, log_level, source, message = self.__parse_line(line)
        with self.__footprints_lock:
          self.__footprints[-1].update(source, log_level, log_file.tell())

  def __parse_line(self, line):
    """
    Parse log line.

    Log line assumed to have the following format:

    SOURCE       LOG_LEVEL     TIMESTAMP        MESSAGE
    [Application] [DEBUG] [2015-05-15 13:34:04] Initialization step 1: configure
    """
    # source
    begin = 1
    end = line.find("]")
    source = unicode(line[begin:end])

    # log level
    begin = end + 3
    end = line.find("]", begin)
    log_level = self.STRING_TO_LOG_LEVEL_MAPPING[line[begin:end]]

    # timestamp
    begin = end + 3
    end = line.find("]", begin)
    timestamp = unicode(line[begin:end])

    # message
    begin = end + 2
    end = -1
    message = unicode(line[begin:end])

    return (timestamp, log_level, source, message)


class Footprint(object):
  """
  Auxiliary structure holding statistics about log file at some position.
  Mentioned statistics includes number of debug, info, warning, error, fatal messages for each source.
  """
  def __init__(self, init=None):
    if init is None:
      self.source_counters = {}
      self.file_offset = 0
    else:
      self.source_counters = dict(init.source_counters)
      self.file_offset = int(init.file_offset)

  def todict(self):
    """
    Get contained data as dictionary.
    """
    result = {}
    result["file_offset"] = self.file_offset
    result["sources"] = []
    for source, counters in self.source_counters.items():
      result["sources"].append({"name": source, "counters": counters})
    return result

  @classmethod
  def fromdict(cls, footprint_dict):
    result = cls()
    result.file_offset = footprint_dict["file_offset"]
    result.source_counters = dict([(source["name"], source["counters"]) for source in footprint_dict["sources"]])
    return result

  def update(self, source, log_level, file_offset):
    """
    Update footprint based on provided info.
    """
    # if there in no such source in counters then add it
    if not source in self.source_counters:
      self.source_counters[source] = [0] * len(LogFileWatcher.STRING_TO_LOG_LEVEL_MAPPING)

    # increase counter for current source and log level
    self.source_counters[source][log_level / 10 - 1] += 1

    # update current file position
    self.file_offset = file_offset

  def subtract_counters(self, footprint):
    """
    Return copy of current footprint subtracted by given one.
    """
    result = Footprint(self)
    for source, counters in footprint.source_counters.iteritems():
      # if there is no such source in this footprint then counter will have negative value
      result.source_counters[source] = map(lambda a, b: a - b,
                                           result.source_counters.get(source, [0] * len(LogFileWatcher.STRING_TO_LOG_LEVEL_MAPPING)),
                                           counters)
    return result

  def total_lines(self, sources, log_level):
    """
    Calculate total number of lines matching specified condition: list of sources and log_level.
    """
    # sum up counters for requested sources
    source_counters = [self.source_counters[source] for source in sources if source in self.source_counters]
    total_counters = map(lambda *args: sum(args), [0] * len(LogFileWatcher.STRING_TO_LOG_LEVEL_MAPPING), *source_counters)

    # finally, calculate total lines of log file matching requested criteria
    total_lines = sum(total_counters[log_level / 10 - 1:])

    return total_lines
