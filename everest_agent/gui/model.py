# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from . import qt

class Model(qt.QtCore.QObject):
  """
  Main data model for application.

  * Holds current agent state (received by polling its contol API) and notifies UI of changes.
  * Exposes other model-like objects (like backend - connector to agent process).

  It may be worth it to hard-proxy every action though this
  class (backend/application operations, config access) to ensure consistency,
  but lets avoid it until really necessary.
  """

  _UPDATE_INTERVAL = 1000

  tasks_changed = qt.Signal()

  def __init__(self, application, options, config, backend):
    """
    Init application model.
    """
    qt.QtCore.QObject.__init__(self)
    self.__application = application
    self.__options = options
    self.__config = config
    self.__backend = backend

    self.__tasks = None
    self.__backend_tasks = None

    self.__update_timer = qt.QtCore.QTimer()
    self.__update_timer.setInterval(self._UPDATE_INTERVAL)
    self.__update_timer.timeout.connect(self.reload)

    self.__backend.running_changed.connect(self.__on_running_changed)

  def tasks_count(self):
    """
    Get number of active tasks.
    """
    if self.__tasks is None:
      return 0
    return len(self.__tasks)

  def get_tasks(self):
    """
    Get all running tasks as dict
    {
      <task_id>*: <task>
    }
    """
    if self.__tasks is None:
      return {}
    return dict(self.__tasks)

  def reload(self):
    """
    Update model state.
    """
    if self.__backend.is_uninitialized or (not self.__backend.is_running):
      return
    tasks = self.__backend.tasks()
    if tasks != self.__backend_tasks:
      self.__backend_tasks = tasks
      self.__tasks = {t["id"]: t for t in tasks}
      self.tasks_changed.emit()

  @property
  def application(self):
    """
    Get application instance.
    """
    return self.__application

  @property
  def options(self):
    """
    Get application options instance.
    """
    return self.__options

  @property
  def config(self):
    """
    Get agent config instance.
    """
    return self.__config

  @property
  def backend(self):
    """
    Get agent connection aka backend instance.
    """
    return self.__backend

  def __on_running_changed(self, status):
    """
    Start/stop data polling on backend status changed.
    """
    if status:
      self.__update_timer.start()
    else:
      self.__tasks = None
      self.__backend_tasks = None
      self.__update_timer.stop()
      self.tasks_changed.emit()

