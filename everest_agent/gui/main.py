# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


import sys
import os
import optparse

from . import application

def parse_args(args):
  """
  Parse & check command line arguments.
  """
  parser = optparse.OptionParser()
  parser.add_option("", "--qt", choices=("pyqt4", "pyside", "auto"), default="auto",
                    help="qt bindings to use")
  parser.add_option("-m", "--minimized", action="store_true", default=False,
                    help="run gui minimized to tray")
  parser.add_option("-c", "--config", type=str, default=None,
                    help="path to config file")
  parser.add_option("-d", "--workdir", type=str, default=None,
                    help="agent working directory")
  options, pos_args = parser.parse_args(args)
  # disallow positional args... if allowed, they will be passed to QApplication
  if len(pos_args) > 1:
    print parser.format_help()
    sys.exit(1)
  return options, pos_args

def main(argv=None):
  """
  Entry point. Create Application object and run it.
  """
  try:
    if argv is None:
      argv = sys.argv
    options, args = parse_args(argv)
    app = application.Application(options, args)
    sys.exit(app.run())
  except Exception as ex:
    sys.exit(2)

if __name__ == '__main__':
  main()
