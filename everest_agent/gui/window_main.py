# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from . import qt

from . import tab_status
from . import tab_tasks
from . import tab_about

class WindowMain(qt.QtGui.QMainWindow):
  """
  GUI main window.
  """
  _MIN_WIDTH = 600
  _MIN_HEIGHT = 400
  _DEFAULT_WIDTH = 800
  def __init__(self, model):
    qt.QtGui.QMainWindow.__init__(self)
    self.__model = model
    # init window properties
    self.setWindowIcon(qt.QtGui.QIcon(self.__model.options.icon_app))
    self.setWindowTitle(qt.QtGui.QApplication.applicationName())
    self.setMinimumSize(self._MIN_WIDTH, self._MIN_HEIGHT)
    self.resize(self._DEFAULT_WIDTH, self._MIN_HEIGHT)
    # init layout
    self.__tab_widget = qt.QtGui.QTabWidget()
    self.__tab_status = tab_status.TabStatus(model)
    self.__tab_tasks = tab_tasks.TabTasks(model)
    self.__tab_about = tab_about.TabAbout(model)
    self.__tab_widget.addTab(self.__tab_status, self.tr("Status"))
    self.__tab_widget.addTab(self.__tab_tasks, self.tr("Tasks"))
    self.__tab_widget.addTab(self.__tab_about, self.tr("About"))
    self.setCentralWidget(self.__tab_widget)
    # listeners
    self.__model.backend.uninitialized_changed.connect(self.__on_agent_uninitialized)

  def __on_agent_uninitialized(self, status):
    """
    Disable the whole window while agent is uninitialized.
    May be overkill, but it will do for now.
    """
    self.setEnabled(not status)
