# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from . import qt

from . import window_configuration

class TrayIcon(qt.QtGui.QSystemTrayIcon):
  """
  System tray icon & menu.
  """

  def __init__(self, model):
    """
    Init new TrayIcon instance.
    """
    qt.QtGui.QSystemTrayIcon.__init__(self, None)
    self.__model = model
    self.__active_icon = qt.QtGui.QIcon(model.options.icon_active)
    self.__idle_icon = qt.QtGui.QIcon(model.options.icon_idle)
    # configure context menu
    #
    self.__menu = qt.QtGui.QMenu()

    self.__open_action = self.__menu.addAction(self.tr("Open..."))
    self.__configure_action = self.__menu.addAction(self.tr("Configure..."))
    self.__toggle_action = self.__menu.addAction("")
    self.__menu.addSeparator()
    self.__exit_action = self.__menu.addAction(self.tr("Exit"))

    self.setContextMenu(self.__menu)
    self.setToolTip(qt.QtGui.QApplication.applicationName())

    self.__open_action.triggered.connect(self.__open_window)
    self.__configure_action.triggered.connect(self.__open_configuration)
    self.__toggle_action.triggered.connect(self.__toggle_agent)
    self.__exit_action.triggered.connect(self.__model.application.close)
    self.__model.backend.running_changed.connect(self.__on_running_changed)
    self.__model.backend.uninitialized_changed.connect(self.__on_uninitialized_changed)
    self.__on_uninitialized_changed(self.__model.backend.is_uninitialized)
    self.__on_running_changed(self.__model.backend.is_running)

  def __open_window(self):
    """
    Open main window.
    """
    self.__model.application.show_main_window()

  def __open_configuration(self):
    """
    Open configuration dialog.
    """
    self.__toggle_action.setEnabled(False)
    self.__configure_action.setEnabled(False)
    window_configuration.WindowConfiguration.show_modal(None, self.__model)
    self.__configure_action.setEnabled(True)
    self.__toggle_action.setEnabled(True)

  def __toggle_agent(self):
    """
    Start/stop the agent.
    """
    if self.__model.backend.is_running:
      self.__model.backend.stop()
    else:
      self.__model.backend.start()

  def __on_running_changed(self, status):
    """
    Update menu on agent start/stop.
    """
    if status:
      icon = self.__active_icon
      toggle_label = self.tr("Stop")
    else:
      icon = self.__idle_icon
      toggle_label = self.tr("Start")
    self.__toggle_action.setText(toggle_label)
    self.__configure_action.setEnabled(not status)
    self.setIcon(icon)

  def __on_uninitialized_changed(self, status):
    """
    Disable dangerous action while agent is starting/stopping.
    """
    self.__toggle_action.setEnabled(not status)
    self.__configure_action.setEnabled(not (self.__model.backend.is_running or status))
