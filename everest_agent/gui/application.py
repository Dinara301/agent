# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


import sys
import os
import signal
import traceback
import json
import logging
import datetime

from . import qt
from . import config
from . import resource

class Application(object):
  """
  Application root. Initializes and binds together top-level application components.
  """
  # external config file (for UI customization)
  _EXTERNAL_CONFIG = "external.json"
  _EXTERNAL_APPNAME = "appName"
  _EXTERNAL_APPNAME_SHORT = "appNameShort"
  _EXTERNAL_VERSION = "version"
  _EXTERNAL_ICONAPP = "iconApp"
  _EXTERNAL_ICONIDLE = "iconIdle"
  _EXTERNAL_ICONACITIVE = "iconActive"
  _EXTERNAL_ABOUT_TEXT = "aboutText"
  _EXTERNAL_ABOUT_IMAGE = "aboutImage"
  # stylesheet file
  _QT_STYLESHEET = "qtstyle.qss"
  # logging configuration
  _LOG_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
  _LOG_FORMAT = "[%(name)s] [%(levelname)s] [%(asctime)s] %(message)s"
  _LOG_ROOT = "logs"
  _LOG_FILENAME_DATE_FORMAT = "%Y-%m-%d_%H-%M-%S"
  # call noop python code from Qt event loop each XXX ms
  _PYTHON_WAKEUP_INTERVAL = 300

  def __init__(self, options, args):
    # before doing anything, setup logging to get maximum debug info if anything fails
    self.__log = self.__setup_logging()
    # store application options
    self.__options = options
    self.__args = args

    # init fields
    # * just to list them
    # * to avoid sudden AttributeErrors
    self.__app = None
    self.__config = None
    self.__backend = None
    self.__model = None
    self.__tray_icon = None
    self.__main_window = None
    self.__timer = None

    init_steps = [
      ("configure", self.__init_config),
      ("enable file logging", self.__init_file_log),
      ("init Qt", self.__init_qt),
      ("init application model", self.__init_model),
      ("init user inteface", self.__init_ui),
      ("install interrupt handler", self.__install_signal_handler),
      ("autostart agent if required", self.__autostart_agent)
    ]

    for idx, (name, func) in enumerate(init_steps):
      try:
        self.__log.debug("Initialization step %d: %s", idx + 1, name)
        func()
      except Exception as ex:
        self.__log.fatal("Failed to %s! Reason: %s", name, ex)
        self.__log.debug(traceback.format_exc())
        # Qt is already initialzed, so let's show error messagebox
        if self.__app is not None:
          message = qt.QtGui.QApplication.translate("App", "Failed to initialize application.\n\n")
          message += qt.QtGui.QApplication.translate("App", "You can try to remove configuration file '{}'.").format(self.__options.config)
          details = traceback.format_exc()
          message_box = qt.QtGui.QMessageBox(qt.QtGui.QMessageBox.Critical, qt.QtGui.QApplication.translate("App", "Error"), message)
          message_box.setDetailedText(details)
          message_box.exec_()
        raise
    self.__log.info("Application initialized successfully")

  def run(self):
    """
    Start the application.
    """
    return self.__app.exec_()

  def close(self, errcode=0):
    """
    Close the application with appropriate exit code.
    """
    try:
      self.__model.backend.stop(True)
      self.__main_window.close()
    except Exception:
      self.__log.error("Graceful shutdown failed")
      self.__log.debug(traceback.format_exc())
    self.__log.info("Exit code: %d", errcode)
    self.__app.exit(errcode)

  def show_main_window(self):
    """
    Show/bring-to-front main window.
    """
    # seems like it cannot be brought to front by conventional means
    if self.__main_window.isVisible():
      self.__main_window.close()
    self.__main_window.show()
    self.__main_window.raise_()

  def __setup_logging(self):
    """
    Setup application logging.
    """
    logging.basicConfig(level=logging.DEBUG, datefmt=self._LOG_DATE_FORMAT, format=self._LOG_FORMAT)
    return logging.getLogger(self.__class__.__name__)

  def __init_config(self):
    """
    Read and process application configuration.
    """
    self.__log.debug("  Application arguments:")
    for option in sorted(self.__options.__dict__.keys()):
      self.__log.debug("    %-15s: %s", option, getattr(self.__options, option))
    # handle customization config
    with open(resource.get(self._EXTERNAL_CONFIG), "r") as external_config_file:
      external_config = json.load(external_config_file)
    try:
      self.__options.app_name = external_config[self._EXTERNAL_APPNAME]
      self.__options.app_name_short = external_config[self._EXTERNAL_APPNAME_SHORT]
      self.__options.app_version = external_config[self._EXTERNAL_VERSION]
      self.__options.icon_app = resource.get(external_config[self._EXTERNAL_ICONAPP])
      self.__options.icon_active = resource.get(external_config[self._EXTERNAL_ICONACITIVE])
      self.__options.icon_idle = resource.get(external_config[self._EXTERNAL_ICONIDLE])
      self.__options.about_text = "\n".join(external_config[self._EXTERNAL_ABOUT_TEXT]).format(appName=self.__options.app_name, appVersion=self.__options.app_version)
      self.__options.about_image = resource.get(external_config[self._EXTERNAL_ABOUT_IMAGE])
    except KeyError:
      self.__log.fatal("  Malformed GUI external config")
      raise

    #
    # add derived parameters & non-declarative defaults
    if self.__options.workdir is None:
      self.__options.workdir = os.path.join(os.path.expanduser("~").decode(sys.getfilesystemencoding()), "." + self.__options.app_name_short)
      self.__log.debug("  No 'workdir' is set, defaulting to %s", self.__options.workdir)
      if not os.path.isdir(self.__options.workdir.encode(sys.getfilesystemencoding())):
        os.makedirs(self.__options.workdir.encode(sys.getfilesystemencoding()))
    if self.__options.config is None:
      self.__options.config = os.path.join(self.__options.workdir, "agent.conf")
      self.__log.debug("  No 'config' is set, defaulting to <workdir>/agent.conf (%s)", self.__options.config)
    # log directory setup
    current_date = datetime.datetime.now()
    log_directory = os.path.join(self.__options.workdir, self._LOG_ROOT, current_date.strftime("%Y-%m-%d"))
    if not os.path.isdir(log_directory.encode(sys.getfilesystemencoding())):
      os.makedirs(log_directory.encode(sys.getfilesystemencoding()))
    timestamp = current_date.strftime("%H-%M-%S")
    self.__options.log_file = os.path.join(log_directory, "{}.txt".format(timestamp))
    self.__options.gui_log_file = os.path.join(log_directory, "{}_gui.txt".format(timestamp))

    self.__log.debug("  Application configuration:")
    for option in sorted(self.__options.__dict__.keys()):
      self.__log.debug("    %-15s: %s", option, getattr(self.__options, option))

  def __init_file_log(self):
    """
    Init file logger.
    """
    # TODO: maybe, cleanup some old logs?
    formatter = logging.Formatter(self._LOG_FORMAT, self._LOG_DATE_FORMAT)
    handler = SingleLineFileHandler(self.__options.gui_log_file.encode(sys.getfilesystemencoding()))
    handler.setFormatter(formatter)
    logging.getLogger().addHandler(handler)
    # just a reminder to oneself...
    self.__log.debug("  File handler installed (Note: early init logs are missing in log file)")

  def __init_qt(self):
    """
    Initialize Qt bindings.
    """
    api_used = qt.init(self.__options.qt)
    self.__log.debug("  Using %s Qt API", api_used)
    self.__options.qt = api_used
    self.__app = qt.QtGui.QApplication(self.__args)
    self.__app.setApplicationName(self.__options.app_name)
    self.__app.setApplicationVersion(self.__options.app_version)
    # we actually exit only by tray menu
    self.__app.setQuitOnLastWindowClosed(False)
    # load stylesheet for UI customization
    with open(resource.get(self._QT_STYLESHEET), "r") as stylesheet:
      self.__app.setStyleSheet(stylesheet.read())
    self.__log.debug("  Loaded custom stylesheet %s", self._QT_STYLESHEET)


  def __init_model(self):
    """
    Initialize application model.
    """
    # sadly, imports are here (as we need to select qt bindings before using them)
    from . import backend
    from . import model
    default_config_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "agent.conf.default")
    self.__log.debug("  Loading default config (%s)", default_config_path)
    with open(default_config_path) as conf_proto:
      basic_config = json.load(conf_proto)
    self.__config = config.Config(basic_config, self.__options.config)
    self.__backend = backend.Backend(self.__config, self.__options)
    self.__model = model.Model(self, self.__options, self.__config, self.__backend)
    # add application-level handler for failed agent start
    self.__backend.start_failed.connect(self.__agent_start_failed)


  def __init_ui(self):
    """
    Create UI elements.
    """
    from . import tray_icon
    from . import window_main
    self.__tray_icon = tray_icon.TrayIcon(self.__model)
    self.__main_window = window_main.WindowMain(self.__model)
    self.__tray_icon.show()
    if not self.__options.minimized:
      self.__main_window.show()

  def __install_signal_handler(self):
    """
    Setup signal handling.
    """
    # when Qt eventloop is running, python signal handlers won't trigger
    # we need to 'ping' interpreter from time to time to avoid this
    self.__timer = qt.QtCore.QTimer()
    self.__timer.start(self._PYTHON_WAKEUP_INTERVAL)
    self.__timer.timeout.connect(lambda: None)
    signal.signal(signal.SIGINT, self.__on_sigint)

  def __autostart_agent(self):
    """
    If corresponding config parameter is set, start on application init.
    """
    if not self.__config.agent_autostart:
      return
    try:
      self.__backend.start()
    except Exception:
      # suppress exception there, just write some log
      # raising it from app init sequence will trigger 'fatal error' handler
      #
      # Backend object itself produces more logs there
      pass

  def __agent_start_failed(self, reason):
    """
    Handle agent start fail (show messagebox).

    As start can be initiated from different places, application seems to be most reasonable to handle the error.
    """
    message = qt.QtGui.QApplication.translate("App", "Failed to start agent.\n\n")
    message += qt.QtGui.QApplication.translate("App", "Please, check the agent ports configuration.")
    message_box = qt.QtGui.QMessageBox(qt.QtGui.QMessageBox.Warning, qt.QtGui.QApplication.translate("App", "Error"), message)
    message_box.setDetailedText(reason)
    message_box.exec_()

  def __on_sigint(self, *args):
    """
    SigInt aka Ctrl+C handler.
    """
    self.__log.info("Interrupted by signal, exiting...")
    # ensure correct thread
    qt.QtCore.QTimer.singleShot(0, self.close)


class SingleLineFileHandler(logging.FileHandler):
  def emit(self, record):
    if record.exc_info:
      record.msg += "\n"
      record.msg += "".join(traceback.format_exception(*record.exc_info))
      record.exc_info = None
    lines = record.msg.split("\n")
    for l in lines:
      record.msg = l
      logging.FileHandler.emit(self, record)

