# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

#
# Launchpad script for to execute agent in a subprocess more reliably (for a GUI)
#
# It is a workaround for strange PYTHONPATH configurations.
#

import os
import sys
import runpy

root = reduce(lambda path, _: os.path.dirname(path), xrange(4), __file__)
sys.path.insert(0, root)

# Set run_name explicitly, as by defauly it would be 'everest_agent.__main__'
runpy.run_module("everest_agent.start", run_name="__main__", alter_sys=True)
