# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import logging

from . import qt
from . import window_log

class TabAbout(qt.QtGui.QWidget):
  """
  About tab.
  """
  def __init__(self, model):
    """
    Init new TabAbout instance.
    """
    qt.QtGui.QWidget.__init__(self)
    self.__model = model

    # init layout
    layout = qt.QtGui.QVBoxLayout()

    image_label = qt.QtGui.QLabel()
    image_label.setPixmap(qt.QtGui.QPixmap(self.__model.options.about_image))
    image_label.setAlignment(qt.QtCore.Qt.AlignCenter)
    text_label = qt.QtGui.QLabel(self.__model.options.about_text)
    text_label.setWordWrap(True)

    layout.addWidget(image_label)
    layout.addWidget(text_label, 1)
    #layout.addStretch(1)
    layout.addLayout(self.__init_buttons())

    self.setLayout(layout)


  def __init_buttons(self):
    """
    Return initialized buttons layout (Help...).
    """
    layout = qt.QtGui.QHBoxLayout()

    button_help = qt.QtGui.QPushButton(self.tr("Help..."))
    button_help.clicked.connect(self.__open_help)

    button_log = qt.QtGui.QPushButton(self.tr("Log..."))
    button_log.clicked.connect(self.__open_log)

    layout.addWidget(button_help)
    layout.addStretch(1)
    layout.addWidget(button_log)
    return layout

  def __open_help(self):
    """
    Open HTML help in system browser.
    """
    pass


  def __open_log(self):
    """
    Open GUI logs.
    """
    window_log.WindowLog.show_modal(self, self.__model.options.gui_log_file, logging.DEBUG)
