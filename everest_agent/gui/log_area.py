# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import logging
import webbrowser

from . import qt
from . import resource

# Valid log levels collection.
# Warning: order is important!
_LOG_LEVELS = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.FATAL]

class LogArea(qt.QtGui.QWidget):
  """
  Logging widget. Shows 'infinite' log and stats.
  """
  _OPEN_EDITOR_ICON = "icon-share.png"

  def __init__(self, title, log_watcher, log_level=logging.INFO):
    """
    Init new LogArea.
    """
    qt.QtGui.QWidget.__init__(self)

    layout = qt.QtGui.QVBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)

    self.__log_watcher = log_watcher
    self.__list_view = qt.QtGui.QListView()
    self.__list_model = _LogModel(log_watcher, log_level)
    self.__list_view.setUniformItemSizes(True)
    self.__list_view.setSelectionMode(qt.QtGui.QAbstractItemView.NoSelection)

    self.__open_editor_button = None
    self.__line_count_label = None

    self.__list_view.setModel(self.__list_model)
    self.__list_model.rowsInserted.connect(self.__autoscroll)

    layout.addLayout(self.__init_header(title, log_level))
    layout.addWidget(self.__list_view)
    self.setLayout(layout)


  def __init_header(self, title, init_log_level):
    """
    Init widget header:
    - title
    - message counter
    - log level filter
    - 'open in external editor' button
    """
    layout = qt.QtGui.QHBoxLayout()

    title_label = qt.QtGui.QLabel(title)

    self.__line_count_label = qt.QtGui.QLabel()
    self.__line_count_label.setToolTip(self.tr("Number of messages matching the current filter"))

    log_level_label = qt.QtGui.QLabel(self.tr("Log level:"))

    log_level_selector = qt.QtGui.QComboBox()
    for log_level in _LOG_LEVELS:
      log_level_selector.addItem(logging.getLevelName(log_level).capitalize(), log_level)
    log_level_selector.setCurrentIndex(_LOG_LEVELS.index(init_log_level))
    log_level_selector.setToolTip(self.tr("Minimal log level to show"))

    self.__open_editor_button = qt.QtGui.QPushButton(qt.QtGui.QIcon(resource.get(self._OPEN_EDITOR_ICON)), "")
    self.__open_editor_button.setToolTip(self.tr("Open log file in external editor"))

    layout.addWidget(title_label)
    layout.addWidget(self.__line_count_label)
    layout.addStretch(1)
    layout.addWidget(log_level_label)
    layout.addWidget(log_level_selector)
    layout.addWidget(self.__open_editor_button)

    log_level_selector.currentIndexChanged.connect(self.__log_level_changed)
    self.__open_editor_button.clicked.connect(self.__open_external_editor)
    self.__list_model.line_count_changed.connect(self.__on_line_count_changed)

    self.__on_line_count_changed(0)

    return layout

  def hideEvent(self, event):
    """
    Stop logs update when widget is invisible.
    """
    self.__list_model.stop_autoupdate()
    super(LogArea, self).hideEvent(event)

  def showEvent(self, event):
    """
    Restart logs update when widget is shown.
    """
    self.__list_model.start_autoupdate()
    super(LogArea, self).showEvent(event)

  def __log_level_changed(self, index):
    """
    Set new log level.
    """
    self.__list_model.set_log_level(_LOG_LEVELS[index])

  def __on_line_count_changed(self, count):
    """
    Update log messages count label.
    """
    self.__line_count_label.setText("({})".format(count))
    self.__open_editor_button.setEnabled(bool(count))

  def __open_external_editor(self):
    filename = self.__log_watcher.filename
    if filename is None:
      return
    webbrowser.open(filename)

  def __autoscroll(self):
    """
    Autoscroll if scroller was at 'bottom' position already.
    """
    scrollbar = self.__list_view.verticalScrollBar()
    if scrollbar.value() == scrollbar.maximum():
      self.__list_view.scrollToBottom()


class _LogModel(qt.QtCore.QAbstractListModel):
  """
  Log model.
  - Adapts 'actual model' (LogFileWatcher) to Qt model-view framework.
  - Performs request caching, so we don't reread lines from disk on every scroll.
  """
  # load this many log entries on direction of the scroll
  _SHIFT_SIZE = 200
  # max log entry cache size
  _MAX_SIZE = 400
  # log file watcher polling interval
  _AUTOUPDATE_INTERVAL = 2000

  line_count_changed = qt.Signal(int)

  def __init__(self, log_watcher, log_level):
    qt.QtCore.QAbstractListModel.__init__(self)
    self.__log_watcher = log_watcher
    self.__total_lines = 0
    self.__log_level = log_level

    self.__update_timer = qt.QtCore.QTimer()
    self.__update_timer.setInterval(self._AUTOUPDATE_INTERVAL)
    self.__update_timer.timeout.connect(self.__update_line_count)

    self.__reset_data()

  def set_log_level(self, level):
    """
    Set log level filter.
    """
    self.__reset_data()
    self.modelReset.emit()
    assert level in _LOG_LEVELS
    self.__log_level = level
    self.__update_line_count()

  def start_autoupdate(self):
    """
    Start model autoupdate.
    """
    self.__update_line_count()
    self.__update_timer.start()

  def stop_autoupdate(self):
    """
    Stop model autoupdate.
    """
    self.__update_timer.stop()

  def rowCount(self, model_index):
    """
    QAbstractListModel interface implementation.
    """
    # for non-tree views, any valid model index must return 0 (meaning 'no children')
    if model_index.isValid():
      return 0
    return self.__total_lines

  def data(self, model_index, role):
    """
    QAbstractListModel interface implementation.
    """
    if not model_index.isValid() or role != qt.QtCore.Qt.DisplayRole or model_index.row() > self.__total_lines:
      return None
    timestamp, level, source, message = self.__get_message_data(model_index.row())
    return "[{}] [{}] [{}] {}".format(source, logging.getLevelName(level), timestamp, message)

  def __update_line_count(self):
    """
    Update model state from LogFileWatcher.
    """
    old_total = self.__total_lines
    self.__footprint = self.__log_watcher.footprint()
    self.__total_lines = reduce(lambda current, source: current + sum(source["counters"][_LOG_LEVELS.index(self.__log_level):]), self.__footprint["sources"], 0)
    changed = self.__total_lines - old_total
    if changed != 0:
      self.line_count_changed.emit(self.__total_lines)
    if changed > 0:
      self.rowsInserted.emit(qt.QtCore.QModelIndex(), old_total, self.__total_lines)
    if changed < 0:
      self.__reset_data()
      self.modelReset.emit()

  def __reset_data(self):
    """
    Reset log lines cache.
    """
    self.__data = []
    self.__data_start = -1
    self.__data_end = -1

  def __get_message_data(self, index):
    """
    Get message data if, reading it from disk if needed.
    """
    # TODO: handle incremental logs more efficiently
    if index >= self.__data_start and index < self.__data_end:
      return self.__data[index - self.__data_start]
    if index < self.__data_start:
      start = max(0, index - self._SHIFT_SIZE)
      end = min(start + self._MAX_SIZE, self.__total_lines)
    if index >= self.__data_end:
      end = min(index + self._SHIFT_SIZE, self.__total_lines)
      start = max(0, end - self._MAX_SIZE)
    self.__data = self.__log_watcher.request_data(start, end - start, [source["name"] for source in self.__footprint["sources"]], self.__log_level, None)
    self.__data_start = start
    self.__data_end = end
    return self.__data[index - self.__data_start]

