# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

_initialized = False
_api = None
QtCore = None
QtGui = None
Signal = None
Slot = None
Property = None

PC_PROPERTY = "priorityCategory"
PC_NEUTRAL, PC_FINE, PC_WARN, PC_FAIL = None, "info", "warn", "error"

INSTRUCTION_PROPERTY = "is_instruction"
INSTRUCTION_TRUE, INSTRUCTION_FALSE = "true", None

def init(api):
  """
  Import & initialize qt bindings.
  """
  if _initialized:
    raise RuntimeError("qt bindings are already initialized")
  assert api in ["pyside", "pyqt4", "auto"]

  if api == "pyside":
    qt = _use_pyside()
  elif api == "pyqt4":
    qt = _use_pyqt()
  else:
    try:
      qt = _use_pyside()
    except ImportError:
      qt = _use_pyqt()
  global _initialized, _api, QtCore, QtGui, Signal, Slot, Property
  _api, QtCore, QtGui, Signal, Slot, Property = qt
  _initialized = True
  return _api

def _use_pyside():
  """
  Import pyside bindings.
  """
  from PySide import QtCore, QtGui
  return "pyside", QtCore, QtGui, QtCore.Signal, QtCore.Slot, QtCore.Property

def _use_pyqt():
  """
  Import & configure pyqt bindings.
  """
  import sip
  api2_classes = ["QData", "QDateTime", "QString", "QTextStream", "QTime", "QUrl", "QVariant"]
  for cls in api2_classes:
    sip.setapi(cls, 2)
  from PyQt4 import QtCore, QtGui
  return "pyqt4", QtCore, QtGui, QtCore.pyqtSignal, QtCore.pyqtSlot, QtCore.pyqtProperty
