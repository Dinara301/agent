# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import copy
import logging
import json

class Config(object):
  """
  Config file wrapper.

  Has 2 main purposes:
  * avoid hardcoding config keys elsewhere (all needed config params are exposed through properties)
  * perform some basic validation
  """
  # agent logfile name
  _LOGFILE_NAME = "agent.txt"
  # formatting indent on dumping to file
  _INDENT = 2

  # default active mode server
  _DEFAULT_ACTIVE_MODE_SERVER = "wss://everest.distcomp.org/compute/ws"

  # 'important' config keys
  _CONTROL_API_ENABLED_PATH = ("controlAPI", "enabled")
  _CONTROL_API_PORT_PATH = ("controlAPI", "port")
  _WHITELIST_PATH = ("security", "allowedCommands")
  _AGENT_AUTOSTART_PATH = ("gui", "autostartAgent")
  _AGENT_ATTRIBUTES_PATH = ("resource", "attributes")
  _AGENT_ACTIVE_MODE_PATH = ("protocol", "activeMode", "enabled")
  _AGENT_SERVER_PATH = ("protocol", "activeMode", "serverURI")
  _AGENT_PASSIVE_MODE_PATH = ("protocol", "passiveMode", "enabled")
  _AGENT_PORT_PATH = ("protocol", "passiveMode", "agentPort")
  _TASK_PROTOCOL_ENABLED_PATH = ("taskProtocol", "enabled")
  _TASK_PORT_PATH = ("taskProtocol", "port")
  _CLIENT_TOKENS_PATH = ("protocol", "passiveMode", "clientTokens")
  _AGENT_TOKEN_PATH = ("protocol", "activeMode", "agentToken")

  def __init__(self, basic_config, path):
    """
    Init new Config instance.
    """
    assert isinstance(basic_config, dict)
    assert isinstance(path, basestring)
    self.__path = path
    self.__data = copy.deepcopy(basic_config)

    # enable settings required by the gui
    self.set(True, self._CONTROL_API_ENABLED_PATH)
    self.set(True, self._TASK_PROTOCOL_ENABLED_PATH)

    self.__log = logging.getLogger(self.__class__.__name__)
    if os.path.isfile(path):
      with open(path) as f:
        try:
          self.__data.update(json.load(f))
        except Exception:
          self.__log.warning("Malformed config file %s, will be overriden by default", self.__path)
      try:
        self.validate()
      except Exception as ex:
        self.__log.warning("Invalid config file %s, will be overriden by default", self.__path)
        self.__data = copy.deepcopy(basic_config)
    else:
      self.__log.info("Config file %s does not exist, creating default config", self.__path)
    self.persist()

  def validate(self):
    """
    Simple config validation.
    """
    if not self.get(self._CONTROL_API_ENABLED_PATH):
      raise Exception("agent control API must be enabled")
    if not (self.active_mode_enabled or self.passive_mode_enabled):
      raise Exception("at least one mode must be enabled")

  def persist(self):
    """
    Save current config state to disk.
    """
    try:
      with open(self.__path, "w") as f:
        json.dump(self.__data, f, indent=self._INDENT)
    except Exception as e:
      self.__log.error("Failed to persist config {}".format(self.__path))
      raise

  @property
  def path(self):
    """
    Get path to config file.
    """
    return self.__path

  @property
  def logfile(self):
    """
    Get agent logfile path (possibly relative).
    """
    return os.path.join(self.get("logging", "logDir"), self._LOGFILE_NAME)

  @property
  def control_port(self):
    """
    Get control API port number.
    """
    return self.get(self._CONTROL_API_PORT_PATH)

  @control_port.setter
  def control_port(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._CONTROL_API_PORT_PATH, int)

  @property
  def active_mode_enabled(self):
    """
    Check if the agent is configured to active mode (connecting to job server)
    """
    return self.get(self._AGENT_ACTIVE_MODE_PATH)

  @active_mode_enabled.setter
  def active_mode_enabled(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._AGENT_ACTIVE_MODE_PATH, bool)

  @property
  def server_uri(self):
    """
    Get active mode active server url.
    """
    return self.get(self._AGENT_SERVER_PATH)

  @property
  def passive_mode_enabled(self):
    """
    Check if the agent is configured to active mode (connecting to job server)
    """
    return self.get(self._AGENT_PASSIVE_MODE_PATH)

  @passive_mode_enabled.setter
  def passive_mode_enabled(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._AGENT_PASSIVE_MODE_PATH, bool)

  @property
  def agent_port(self):
    """
    Get agent API port number.
    """
    return self.get(self._AGENT_PORT_PATH)

  @agent_port.setter
  def agent_port(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._AGENT_PORT_PATH, int)

  @property
  def whitelist(self):
    """
    Get allowed commands list.
    """
    return self.get(self._WHITELIST_PATH)

  @whitelist.setter
  def whitelist(self, value):
    """
    Property setter.
    """
    if value is None:
      self.set(value, self._WHITELIST_PATH)
      return
    self.__set_and_check(value, self._WHITELIST_PATH, list)

  @property
  def agent_autostart(self):
    """
    If true, GUI should start agent on init.
    """
    return self.get(self._AGENT_AUTOSTART_PATH)

  @agent_autostart.setter
  def agent_autostart(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._AGENT_AUTOSTART_PATH, bool)

  @property
  def agent_attributes(self):
    """
    Return custom agent attributes dictionary.
    """
    return self.get(self._AGENT_ATTRIBUTES_PATH)

  @agent_attributes.setter
  def agent_attributes(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._AGENT_ATTRIBUTES_PATH, dict)

  @property
  def task_port(self):
    """
    Get agent API port number.
    """
    return self.get(self._TASK_PORT_PATH)

  @task_port.setter
  def task_port(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._TASK_PORT_PATH, int)

  @property
  def client_tokens(self):
    """
    Get list of valid client tokens.
    """
    return self.get(self._CLIENT_TOKENS_PATH)

  @client_tokens.setter
  def client_tokens(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._CLIENT_TOKENS_PATH, list)

  @property
  def agent_token(self):
    """
    Get agent token (used to auth on remote server in active mode).
    """
    return self.get(self._AGENT_TOKEN_PATH)

  @agent_token.setter
  def agent_token(self, value):
    """
    Property setter.
    """
    self.__set_and_check(value, self._AGENT_TOKEN_PATH, basestring)


  def get(self, *path):
    """
    Get particular config value/group.

    Can be called either as config.get("parent1", "parent2", "leaf") or config.get(["parent1", "parent2", "leaf"]).
    """
    path = self.__get_path(path)
    try:
      return reduce(lambda base, value: base[value], path, self.__data)
    except KeyError:
      raise Exception("no {} key in config!", "/".join(path))

  def set(self, value, *path):
    """
    Set config value. Works only for existing keys.
    """
    path = self.__get_path(path)
    parent = self.get(path[:-1])
    leaf_name = path[-1]
    if leaf_name not in parent:
      raise Exception("no {} key in config!", "/".join(path))
    parent[leaf_name] = value

  def __set_and_check(self, value, path, types):
    """
    Validate value and store it to config.
    """
    self.__check_type(value, types, path)
    self.set(value, path)

  def keys(self):
    """
    Get all config keys (not only top-level).
    """
    result = []
    self.__keys([], self.__data, result)
    return result

  def __keys(self, path, node, result):
    """
    Recursive .keys implemetation.
    """
    assert isinstance(node, dict)
    for key, value in node.items():
      valuepath = path + [key]
      if isinstance(value, dict):
        self.__keys(valuepath, value, result)
      else:
        result.append(valuepath)

  def __get_path(self, path):
    """
    Aux - helper for more 'pythonic' signature for get set/methods.
    """
    if len(path) == 1 and (not isinstance(path[0], basestring)):
      path = path[0]
    if not all([isinstance(p, basestring) for p in path]):
      raise TypeError("All path elements must be strings")
    return path

  def __check_type(self, value, types, path):
    """
    Validate value type.
    """
    if not isinstance(value, types):
      if isinstance(types, type):
        types = [types]
      path = "/".join(path)
      type_names = " or ".join([t.__name__ for t in types])
      raise TypeError("wrong '{}' value type ({} instead of {})".format(path, value.__class__.__name__, type_names))
