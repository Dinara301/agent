# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import copy
from . import qt

class WindowListEdit(qt.QtGui.QDialog):
  _MIN_WIDTH = 480
  _MIN_HEIGHT = 320

  @staticmethod
  def show_modal(parent, data, config):
    """
    Show new editor window (synchronously).

    Returns final state if user pressed 'OK' or initial state if user pressed 'Cancel'.
    """
    dialog = WindowListEdit(parent, data, config)
    dialog.exec_()
    result = dialog.get_result()
    dialog.deleteLater()
    return result

  def get_result(self):
    """
    Get dialog result.

    If user hasn't pressed OK, returns unchanged initial state.
    """
    if self.result() == qt.QtGui.QDialog.Accepted:
      return self.__table_model.get_data()
    return self.__initial_data

  def __init__(self, parent, data, config):
    qt.QtGui.QDialog.__init__(self, parent)

    self.__check_config(config)
    self.__check_data(data)

    self.__initial_data = data
    self.__config = config

    self.setWindowTitle(config.get("window_title", self.tr("List edit")))
    self.setMinimumSize(self._MIN_WIDTH, self._MIN_HEIGHT)

    layout = qt.QtGui.QVBoxLayout()
    layout.addLayout(self.__init_header())
    layout.addWidget(self.__init_table(), 1)
    layout.addLayout(self.__init_buttons())
    self.setLayout(layout)

  def __init_header(self):
    """
    Get main editable table.
    """
    layout = qt.QtGui.QHBoxLayout()

    self.__button_add = qt.QtGui.QPushButton(self.tr("Add"))
    self.__button_add.clicked.connect(self.__add_row)
    self.__button_remove = qt.QtGui.QPushButton(self.tr("Remove"))
    self.__button_remove.setEnabled(False)
    self.__button_remove.clicked.connect(self.__remove_row)

    layout.addStretch(1)
    layout.addWidget(self.__button_add)
    layout.addWidget(self.__button_remove)

    return layout

  def __init_table(self):
    """
    Get main table.
    """
    self.__table = qt.QtGui.QTableView()

    self.__table_model = _TableModel(self.__initial_data, self.__config)

    self.__table.setModel(self.__table_model)
    self.__table.horizontalHeader().setStretchLastSection(True)
    self.__table.verticalHeader().hide()
    self.__table.setSelectionMode(qt.QtGui.QAbstractItemView.ExtendedSelection)
    self.__table.setSelectionBehavior(qt.QtGui.QAbstractItemView.SelectRows)
    # leads to crash as a oneliner... (selectionModel().selectionChanged...)
    #
    # looks like PySide/PyQt memory management problem which I've already seen with things like QFuture
    model = self.__table.selectionModel()
    model.selectionChanged.connect(self.__table_selection_changed)

    return self.__table

  def __init_buttons(self):
    """
    Get ok/cancel buttons layout.
    """
    layout = qt.QtGui.QHBoxLayout()

    button_ok = qt.QtGui.QPushButton(self.tr("OK"))
    button_ok.setDefault(True)
    button_cancel = qt.QtGui.QPushButton(self.tr("Cancel"))

    button_ok.clicked.connect(self.accept)
    button_cancel.clicked.connect(self.reject)

    layout.addStretch(1)
    layout.addWidget(button_ok)
    layout.addWidget(button_cancel)

    return layout

  def __check_config(self, config):
    """
    Very simple config validation.

    Doesn't aim at beautiful error messages, just force exception in c-tor rather then somewhere later.
    """
    assert isinstance(config, dict)
    assert set(config.keys()).issubset({"columns", "window_title"})
    assert isinstance(config.get("columns"), list)
    for c in config["columns"]:
      assert set(c.keys()).issubset({"name", "validate"})
      assert isinstance(c.get("name"), basestring)
      validate_config = c.get("validate", {})
      assert isinstance(validate_config, dict)
      assert validate_config.get("type", "none").lower() in ["none", "unique"]
      if validate_config.get("type", "none").lower() != "none":
        assert isinstance(validate_config.get("message"), basestring)

  def __check_data(self, data):
    assert isinstance(data, list)
    for row in data:
      assert isinstance(row, list)
      for cell in row:
        assert isinstance(cell, basestring)

  def accept(self):
    table_data = self.__table_model.get_data()
    for idx, column in enumerate(self.__config["columns"]):
      validate_config = column.get("validate", {"type": "none"})
      if validate_config["type"].lower() == "unique":
        names = [it[idx] for it in table_data]
        if len(names) != len(set(names)):
          qt.QtGui.QMessageBox.warning(self, self.tr("Invalid configuration"), validate_config["message"])
          return
    super(WindowListEdit, self).accept()

  def __table_selection_changed(self, selected, deselected):
    self.__button_remove.setEnabled(len(selected.indexes()) > 0)

  def __add_row(self):
    self.__table_model.add_new_row()

  def __remove_row(self):
    model = self.__table.selectionModel()
    for row_index in sorted(set(map(qt.QtCore.QModelIndex.row, model.selection().indexes())), reverse=True):
      self.__table_model.remove_row(row_index)


class _TableModel(qt.QtCore.QAbstractTableModel):
  def __init__(self, data, config):
    qt.QtCore.QAbstractTableModel.__init__(self)
    self.__data = copy.deepcopy(data)
    self.__config = config

  def rowCount(self, model_index):
    """
    QAbstractTableModel interface implementation.

    Get number of rows.
    """
    # for non-tree views, any valid model index must return 0 (meaning 'no children')
    if model_index.isValid():
      return 0
    return len(self.__data)

  def columnCount(self, model_index):
    """
    QAbstractTableModel interface implementation.

    Get number of columns.
    """
    # for non-tree views, any valid model index must return 0 (meaning 'no children')
    if model_index.isValid():
      return 0
    return len(self.__config["columns"])

  def headerData(self, section, orientation, role):
    """
    QAbstractTableModel interface implementation.

    Return column header.
    """
    if role == qt.QtCore.Qt.TextAlignmentRole:
      return qt.QtCore.Qt.AlignLeft
    if role != qt.QtCore.Qt.DisplayRole or orientation != qt.QtCore.Qt.Horizontal:
      return None
    return self.__config["columns"][section]["name"]

  def data(self, model_index, role):
    """
    QAbstractTableModel interface implementation.

    Return data content of a cell.
    """
    if not model_index.isValid() or role not in [qt.QtCore.Qt.EditRole, qt.QtCore.Qt.DisplayRole]:
      return None
    return self.__data[model_index.row()][model_index.column()]

  def flags(self, model_index):
    """
    QAbstractTableModel interface implementation.

    Return cell flags (editable, selectable etc)
    """
    return qt.QtCore.Qt.ItemIsEditable | qt.QtCore.Qt.ItemIsSelectable | qt.QtCore.Qt.ItemIsEnabled

  def setData(self, index, value, role):
    """
    QAbstractTableModel interface implementation.

    Return data content of a cell.
    """
    self.__data[index.row()][index.column()] = value
    self.dataChanged.emit(index, index)
    return True

  def add_new_row(self):
    """
    Add new empty/default row.
    """
    self.beginInsertRows(qt.QtCore.QModelIndex(), len(self.__data), len(self.__data))
    self.__data.append([c["name"] for c in self.__config["columns"]])
    self.endInsertRows()

  def remove_row(self, index):
    """
    Remove row by index.
    """
    self.beginRemoveRows(qt.QtCore.QModelIndex(), index, index)
    self.__data.pop(index)
    self.endRemoveRows()

  def get_data(self):
    """
    Get current model state.
    """
    return self.__data

