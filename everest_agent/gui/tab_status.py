# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from . import log_area
from . import window_configuration

from . import qt

class TabStatus(qt.QtGui.QWidget):
  """
  Status aka main tab.
  """
  def __init__(self, model):
    """
    Init new TabStatus instance.
    """
    qt.QtGui.QWidget.__init__(self)
    self.__model = model

    layout = qt.QtGui.QVBoxLayout()

    self.__log_area = log_area.LogArea(self.tr("Event log"), self.__model.backend.log_file_watcher)

    status_layout = qt.QtGui.QHBoxLayout()
    server_label = qt.QtGui.QLabel(self.tr("Server:"))
    self.__server_text = qt.QtGui.QLineEdit()
    self.__server_text.setReadOnly(True)
    endpoint_label = qt.QtGui.QLabel(self.tr("Endpoint:"))
    self.__endpoint_text = qt.QtGui.QLineEdit()
    self.__endpoint_text.setReadOnly(True)
    status_layout.addWidget(server_label)
    status_layout.addWidget(self.__server_text, 3)
    status_layout.addStretch(1)
    status_layout.addWidget(endpoint_label)
    status_layout.addWidget(self.__endpoint_text, 3)
    status_layout.setSpacing(5)
    status_layout.setContentsMargins(0, 0, 0, 0)

    footer_layout = qt.QtGui.QHBoxLayout()
    self.__button_configure = qt.QtGui.QPushButton(self.tr("Configure..."))
    self.__status_indicator = qt.QtGui.QLabel()
    self.__status_indicator.setAlignment(qt.QtCore.Qt.AlignCenter)
    self.__button_start_stop = qt.QtGui.QPushButton()
    footer_layout.addWidget(self.__button_configure)
    footer_layout.addWidget(self.__status_indicator, 1)
    footer_layout.addWidget(self.__button_start_stop)
    footer_layout.setSpacing(0)
    footer_layout.setContentsMargins(0, 0, 0, 0)

    layout.addWidget(self.__log_area, 1)
    layout.addLayout(status_layout)
    layout.addLayout(footer_layout)

    self.setLayout(layout)

    self.__button_configure.clicked.connect(self.__open_configuration)
    self.__button_start_stop.clicked.connect(self.__toggle_agent)
    self.__model.backend.running_changed.connect(self.__update_state)

    self.__update_state(self.__model.backend.is_running)

  def __open_configuration(self):
    """
    Open configuration dialog.
    """
    window_configuration.WindowConfiguration.show_modal(self, self.__model)

  def __toggle_agent(self):
    """
    Start/stop the agent.
    """
    if self.__model.backend.is_running:
      self.__model.backend.stop()
    else:
      self.__model.backend.start()

  def __update_state(self, running):
    """
    Update tab on agent start/stop.
    """
    if running:
      status_label = self.tr("Running")
      status_category = qt.PC_FINE
      startstop_label = self.tr("Stop")
      startstop_tooltip = self.tr("Stop the agent and kill all running tasks")
      configure_tooltip = self.tr("You must stop the agent to reconfigure it")
      if self.__model.config.active_mode_enabled:
        server_text = self.__model.config.server_uri
      else:
        server_text = self.tr("Active mode disabled")
      if self.__model.config.passive_mode_enabled:
        endpoint_text = self.__model.backend.endpoint
      else:
        endpoint_text = self.tr("Passive mode disabled")
    else:
      status_label = self.tr("Stopped")
      status_category = qt.PC_NEUTRAL
      startstop_label = self.tr("Start")
      startstop_tooltip = self.tr("Start the agent")
      configure_tooltip = self.tr("Open agent configuration window")
      endpoint_text = server_text = self.tr("Agent is not running")
    self.__status_indicator.setText(status_label)
    self.__status_indicator.setProperty(qt.PC_PROPERTY, status_category)

    # ugly kludge from Qt docs to update element style on custom property change
    self.__status_indicator.style().unpolish(self.__status_indicator)
    self.__status_indicator.style().polish(self.__status_indicator)
    self.__status_indicator.update()

    self.__button_configure.setEnabled(not running)
    self.__server_text.setEnabled(running and self.__model.config.active_mode_enabled)
    self.__endpoint_text.setEnabled(running and self.__model.config.passive_mode_enabled)
    self.__server_text.setText(server_text)
    self.__endpoint_text.setText(endpoint_text)
    self.__button_start_stop.setText(startstop_label)
    self.__button_start_stop.setToolTip(startstop_tooltip)
    self.__button_configure.setToolTip(configure_tooltip)

