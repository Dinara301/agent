# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import sys

# resource dir name
_RESOURCE_DIR = "resource"

def get(path):
  """
  Get full resource path.
  """
  if not os.path.isabs(path):
    abspath = os.path.join(os.path.dirname(os.path.abspath(__file__)), _RESOURCE_DIR, path)
  else:
    abspath = path
  if not os.path.isfile(abspath):
    raise IOError("resource '{}' is not found".format(path))
  return abspath.encode(sys.getfilesystemencoding())
