# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import logging
import traceback

from tornado import gen

from constants import FILE_CHUNK_SIZE

class FileBodyProducer(object):
    def __init__(self, fname):
        self.fname = fname

    @gen.coroutine
    def produce(self, write):
        with open(self.fname, 'rb') as f:
            while True:
                buf = f.read(FILE_CHUNK_SIZE)
                if buf == '':
                    break
                yield write(buf)

def uploadFile(client, uri, fname, **kwargs):
    producer = FileBodyProducer(fname)
    if not 'headers' in kwargs:
        kwargs['headers'] = {}
    kwargs['headers']['Content-Length'] = str(os.path.getsize(fname))
    return client.fetch(uri, body_producer=producer.produce, **kwargs)

class SingleLineFileHandler(logging.FileHandler):
    def emit(self, record):
        if record.exc_info:
            record.msg += "\n"
            record.msg += "".join(traceback.format_exception(*record.exc_info))
            record.exc_info = None
        lines = record.msg.split("\n")
        for l in lines:
            record.msg = l
            logging.FileHandler.emit(self, record)

def setupLoggers(config, logPath = None):
    if logPath is None:
        if not os.path.exists(config.get('logDir')):
            os.makedirs(config.get('logDir'))
        logFileName = os.path.join(config.get('logDir'), 'agent.txt')
    else:
        logFileName = logPath
    
    # disable RotatingFileHandler on Windows because the file handle gets 
    # inherited by child processes which blocks the rollover
    # for multiprocessing module it's fixed in python 3.4
    # for subprocess it's inherited every time because stdout/stderr are redirected
    if config.get('logRotate') and logPath is None and os.name != 'nt':
        fh = logging.handlers.RotatingFileHandler(
            logFileName, maxBytes=config.get('logMaxBytes'),
            backupCount=config.get('logBackupCount'))
    else:
        fh = SingleLineFileHandler(logFileName)    

    logLevel = logging.getLevelName(config.get('logLevel'))

    fh.setLevel(logLevel)
    formatter = logging.Formatter('[%(name)s] [%(levelname)s] [%(asctime)s] %(message)s')
    fh.setFormatter(formatter)

    logging.getLogger('tornado').addHandler(fh)
    logging.getLogger('everest_agent').addHandler(fh)

    logging.getLogger('tornado').setLevel(logLevel)
    logging.getLogger('everest_agent').setLevel(logLevel)
    # prevent GET 200/OK spam
    logging.getLogger('tornado.access').setLevel(logging.DEBUG)

    #logger = logging.getLogger("everest_agent.sender")
    #logger.setLevel(logging.INFO) #Otherwise spam too much
