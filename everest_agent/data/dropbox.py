import os
import json
import logging
import everest_agent.exceptions
import everest_agent.utils
from ConfigParser import ConfigParser #configparser
from tornado.httpclient import AsyncHTTPClient
from everest_agent.exceptions import TaskException
from tornado import gen

logger = logging.getLogger('everest_agent.agent')


def readDropboxConf(filepath):
    config = ConfigParser()
    try:
        with open(filepath, "r") as f:
            config.readfp(f)
            access_token = config.get('app', 'token')
            return access_token
    except Exception, e:
        logger.exception("Configuration file is missed")
        raise everest_agent.exceptions.TaskException("Configuration file is missed: %s" % e)


def downloadDropboxFile(path, f, maxBodySize):
    try:
        path_conf= os.path.join('conf', 'dropbox.conf')
        token = readDropboxConf(path_conf)
        filename = os.path.basename(path)
        client = AsyncHTTPClient(max_buffer_size=maxBodySize)
        url = 'https://content.dropboxapi.com/2/files/download'
        headers = {'Content-Type': '', 'Authorization': 'Bearer %s' % token, 'Dropbox-API-Arg': '{"path":"%s"}' % path}
        future = client.fetch(
            url, method='POST', streaming_callback=f.write, headers=headers, body='',
            validate_cert=False,connect_timeout=120, request_timeout=120)
    except Exception:
        logger.exception("Unable to upload output to Dropbox")
        raise TaskException("Unable to upload %s to %s" % (
            filename, url))
    return future


@gen.coroutine
def uploadDropboxFile(file_path, file_name, path_upload, maxBodySize):
    try:
        path_conf = os.path.join('conf', 'dropbox.conf')
        token = readDropboxConf(path_conf)
        url = 'https://content.dropboxapi.com/2/files/upload'
        dropb_api = json.dumps({'path': path_upload, 'mode': 'add', 'autorename': True, 'mute': False})
        headers = {'Content-Type': 'application/octet-stream', 'Authorization': 'Bearer %s' % token,
                   'Dropbox-API-Arg': dropb_api}
        client = AsyncHTTPClient(max_buffer_size=maxBodySize)
        response = yield everest_agent.utils.uploadFile(
            client, url, file_path,
            method='POST', headers=headers)
    except Exception:
        logger.exception("Unable to upload output to Dropbox")
        raise TaskException("Unable to upload %s to %s" % (
            file_name, url))
    file_uri = "dropbox:/" + json.loads(response.body)['path_display']
    result = {'path' : file_name, 'uri' : file_uri}
    raise gen.Return(result)
