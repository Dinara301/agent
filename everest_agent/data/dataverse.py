import json
import logging
from tornado.httpclient import HTTPClient
from everest_agent.exceptions import TaskException

logger = logging.getLogger('everest_agent.agent')


def makeURI(doi):
    url = 'https://dataverse.harvard.edu/api/datasets/:persistentId/versions/:latest/files?persistentId=' + doi
    client = HTTPClient()
    try:
        response = client.fetch(url)
        data = json.loads(response.body)['data']
        id_str = []
        for item in data:
            datafile = item['datafile']
            id = datafile['id']
            id_str.append(str(id))
        res_uri = 'https://dataverse.harvard.edu/api/access/datafiles'+'/'+(',').join(id_str)
        return res_uri
    except Exception, e:
        logger.exception("Failed to get data from Dataverse: %s" % e.message)
        raise TaskException("Unable to find requested DOI: %s" % e)
