#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import signal
import sys
import json
import os
import time
import logging
import argparse
import socket

from tornado import ioloop
from tornado import gen

from everest_agent.web import makeControlApp, makeMainServer
from everest_agent.agent import Agent
from everest_agent import utils
from everest_agent.config import Config
from everest_agent import exceptions

def signal_handler(signum, frame):
    ioloop.IOLoop.instance().stop()

def tryLoadConfig(config, path):
    try:
        config.update(path)
    except IOError:
        pass
        #print('Failed load configuration from %s' % path)

def makeParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', default = None,
                        help = 'specify a configuration file. If set, default configuration file locations are ignored')
    parser.add_argument('-l', '--log', default = None,
                        help = 'log file path. If set, all logging parameters except level are ignored')
    return parser

def init_agent():
    parser = makeParser()
    args = parser.parse_args()

    try:
        config = Config()
        if args.config:
            tryLoadConfig(config, args.config)
        else:
            if os.name == 'posix':
                tryLoadConfig(config, '/etc/everest_agent/agent.conf')
            tryLoadConfig(config, os.path.expanduser(
                os.path.join('~', '.everest_agent', 'agent.conf')))
            tryLoadConfig(config, os.path.join('conf', 'agent.conf'))

        config.check()
    except exceptions.ConfigException, e:
        print("Error loading configuration, %s" % e)
        sys.exit(1)

    utils.setupLoggers(config, args.log)

    ag = Agent(config)

    if config.useTaskProtocol():
        try:
            ag.startTaskMessageServer()
        except socket.error, e:
            ag.logger.error('Error starting TaskMessage server: %s' % e)
            sys.exit(1)

    try:
        makeControlApp(config, ag)
    except socket.error, e:
        ag.logger.error('Error starting Control API server: %s' % e)
        sys.exit(1)

    try:
        makeMainServer(config, ag)
    except socket.error, e:
        ag.logger.error('Error starting WebSocket server: %s' % e)
        sys.exit(1)

    if config.useActiveMode():
        ioloop.IOLoop.instance().add_future(gen.moment, lambda f: ag.ws_client_loop())

    return ag

def run_loop(ag):
    ioloop.IOLoop.instance().start()
    ag.shutdown()
    logging.shutdown()

def main():
    signal.signal(signal.SIGINT, signal_handler)

    ag = init_agent()

    # Hack for signal handling on Windows
    if os.name == 'nt':
        import threading
        p = threading.Thread(target=run_loop, name='Agent', args=(ag,))
        p.daemon = True
        p.start()
        try:
            while True:
                time.sleep(10)
        except IOError:
            pass
    elif os.name == 'posix':
        run_loop(ag)

if __name__ == '__main__':
    main()
