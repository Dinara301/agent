# Running test suite

* Install [psutil](https://github.com/giampaolo/psutil) library
* Go to AGENT_HOME/test directory
* Run the following command:
    * Windows: `runtests.bat`
    * Linux or Mac OS X: `runtests.sh`

# Running test client

Firstly configure an agent:

* Copy `everest_agent/agent.conf.default` to `conf/agent-test.conf` and edit it.
* Set `protocol.activeMode.enabled` to `false`.
* Set `protocol.passiveMode.clientTokens` to `["12345"]`.

Then start the agent:
```
bin/start.sh -c conf/agent-test.conf
```

Then start the client in another terminal window:
```
python -m everest_agent.test.client localhost:8899 1:12345 /bin/hostname
```

The task's command output will be saved to `stdout` file.