from setuptools import setup, find_packages

from everest_agent.constants import AGENT_VERSION

with open('README.md') as f:
    long_description = f.read()

setup(
    name='everest_agent',
    version=AGENT_VERSION,
    description='Agent software for the Everest cloud platform',
    long_description=long_description,
    url='http://everest.distcomp.org',
    author='IITP RAS',
    author_email='sasmir@gmail.com',
    license='http://www.apache.org/licenses/LICENSE-2.0',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ],
    packages=['everest_agent', 'everest_agent.test', 'everest_agent.gui', 'everest_agent.task_handlers'],
    install_requires=['tornado>=4.1', 'psutil'],
    package_data={
        'everest_agent.test': [
            'config.example',
            'runtests.sh',
            'runtests.bat'
        ]
    }
)
